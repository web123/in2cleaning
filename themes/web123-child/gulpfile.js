var gulp         = require('gulp');
var sass         = require('gulp-sass');
var livereload   = require('gulp-livereload');
var autoprefixer = require('autoprefixer');
var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var imagemin     = require('gulp-imagemin');
var pngquant     = require('imagemin-pngquant');
var uglify       = require('gulp-uglifyjs');


gulp.task('sass', function() {
    gulp.src('./build/sass/**/*.scss')
    .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'nested'}).on('error', sass.logError))
        .pipe(postcss([ autoprefixer({browsers: ['last 2 version'] }) ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('.'))
});


gulp.task('uglify', function() {
  gulp.src('./build/js/**/*.js')
    .pipe(uglify('web123-child.min.js'))
    .pipe(gulp.dest('./js'))
});


gulp.task('serve', function(){
  livereload.listen();
  gulp.watch('build/sass/**/*.scss', ['sass']);
  gulp.watch('build/js/*.js', ['uglify']);
  gulp.watch(['./style.css', './*.php', './js/*.js'], function (files){
      livereload.changed(files)
  });

});

gulp.task('default', ['serve']);
