<?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );



function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');


/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function wpmudev_remove_version() {
return '';
}
add_filter('the_generator', 'wpmudev_remove_version');




function show_loop_blog1(){

ob_start();


include( get_theme_file_path().'/loop_blog.php' ); 

$cont=ob_get_contents();
ob_end_clean();

return $cont;
}


add_shortcode( 'showblogs1', 'show_loop_blog1' );


add_filter( 'template_include', 'include_template_function1', 1 );

function include_template_function1( $template_path ) {
    if ( get_post_type() == 'post' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single_blog.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = get_theme_file_path().'/loop_blog.php';
            }
        }
    }
    return $template_path;
}

// Unset URL from comment form
function crunchify_move_comment_form_below( $fields ) { 
    $comment_field = $fields['comment']; 
    unset( $fields['comment'] ); 
    unset( $fields['url'] ); 
    
    $fields['comment'] = $comment_field;

    return $fields; 
} 
add_filter( 'comment_form_fields', 'crunchify_move_comment_form_below' );

// Add placeholder for Name and Email
function modify_comment_form_fields($fields){
   

   $fields['subject'] = '<p class="comment-form-url">' .
   '<label for="url">' . __( 'Subject', 'domainreference' ) . '</label>' .
       '<input id="subject" name="subject" placeholder="" type="text" value="' . esc_attr( $commenter['comment_author_subject'] ) . '" size="30" /> ' .
      
             '</p>';
    return $fields;
}
add_filter('comment_form_default_fields','modify_comment_form_fields');
