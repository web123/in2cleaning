<?php
/**
 * Template Name:Custom Blog Single Page
 *
 */
get_header(); ?>

<style type="text/css">.i001-extras0 {
    float: right;
    background: #d9f3f8;
    border-radius: 4px;
    padding: 7px 12px;
}
.title1{color: #00acd2 ! important;
    font-size: 38px !important;
    font-weight: 100 !important;}
    .date1{font-family: 'open sans', sans-serif!important;
    color: #000!important;
    font-size: 16px!important;
    line-height: 1.4!important;
    font-weight: 400!important;}
    .banner1 {
    background-image: url(http://www.3ilogics.com/matt/in2c/wp-content/uploads/2017/07/slider1.jpg);
    height: 444px;
    width: 1366px!important;
    position: relative;
    right: 86px;
}
.t115-content {
    padding-top: 47px;
}
.i001-comments-info h3 {
    font-size: 16px!important;
}
h3#reply-title {
    font-size: 16px;
}
.i001-css-button.new_v01 {
    top: 13px;
    position: relative;
    color: #ffffff;
    font-size: 12px;
    text-decoration: none;
    background-color: #5aab1e;
    background-image: linear-gradient(#5aab1e, #6ab334);
    padding: 5px 10px;
    border-radius: 3px;
}
</style>
<?php
global $post;
$mypost = array('post_type' => 'post',);
$loop = new WP_Query($mypost);
//echo "<pre>"; print_r($post);
?>
<div class="wpb_wrapper">
    <?php echo do_shortcode('[rev_slider alias="new_home"]'); ?>
  </div>
<div class="custom__blogs_h container_inner">
   <div class="t115-wrapper empty ">
   	<!-- <div class="banner1"></div> -->

      <div class="t115-content">
      	<div class="i001-extras0"><div class="i001-extras1"><div class="i001-extras2"><div class="i001-extras3">Comments: <span><?php echo $my_var = get_comments_number($post->ID, 'full'); ?></span>
</div></div></div></div>
         <!-- <div class=".i001-extras0">Comments: <span> <?php echo $my_var = get_comments_number($post->ID, 'full'); ?></span> </div> -->
         <div class="i001-detail i001-image-right i001-image-med" id="i001-238321">
            <h1 class="title1" style="padding-bottom: 10px"><?php the_title(); ?></h1>
            <h5 class="date1" style="padding-bottom: 20px"><?php echo date('j-n-Y h:i A', strtotime($post->post_date));  ?></h5>
            <div class="i001-detail-image">
               <div class="img">
                  <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
               </div>
            </div>
            <div class="i001-detail-wrap">
            	<p><?php 
							$content = get_the_content();
							$content = preg_replace("/<img[^>]+\>/i", " ", $content);          
							$content = apply_filters('the_content', $content);
							$content = str_replace(']]>', ']]>', $content);
							echo  $content; ?> </p>
               <?php //echo $post->post_content; ?>
            </div>
         </div>
      </div>
   </div>
   <div class="i001-comments-info" style="position: relative; margin-top: 50px; margin-bottom: 50px;">
      
            
               <div class="i001-extras0"><div class="i001-extras1"><div class="i001-extras2"><div class="i001-extras3">Comments: <span><?php echo $my_var = get_comments_number($post->ID, 'full'); ?></span>
</div></div></div></div>
           
      
      <h3 style="font-size: 38px !important;font-weight: 100 !important;color: #000000;">Comments</h3>
      <a href="javascript:void(0)" id="clickme" class="i001-css-button new_v01">Make a Comment</a>
   </div>
   <?php comment_form(); ?>
   <?php 
      $post_id = $post->ID;
      $comments = get_comments('post_id= '.$post_id.'');
      //echo "<pre>"; print_r($comments); 
      foreach($comments as $comment) :
      	
      ?>
   <div class="i001-comments-item ">
      <h4><?php echo($comment->comment_author); ?></h4>
      <h5> <?php echo date('j-n-Y h:i A', strtotime($comment->comment_date)); ?> - <?php echo($comment->comment_author); ?></h5>
      <?php echo($comment->comment_content); ?>
   </div>
   <?php 
      endforeach; ?>
</div>
<?php wp_reset_query(); ?>


<?php get_footer(); ?>

