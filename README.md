# Web123 - Wordpress starter pack


This pack is designed to get you up and running fast.

### The Theme
This is the Web123 standard theme. We require all external developers to use this theme. If you feel that it is missing key functionality please contact us at [technical@web123.com.au](mailto:technical@web123.com.au) so that we can prepare the necessary changes for you.

If you are making changes to the parent theme please note that your changes may be over written. Please make any core changes in the child theme.

### Plugins
All plugins will be licensed prior to deployment, please make sure all temporary API or license keys are removed before hand over.

Do not delete the /plugins/worker /plugins/wordfence and /plugins/wp-sync-db-1.5 folders.

- manageWP - syncs wordpress manager back to base
- wordfence - security and firewall
- wp-sync-db-1.5 - tool for syncing DB between environments

#### Other Plugins that are included

This starter pack has loads of up-to-date plugins that may be useful for your project. Here's a list of what's included...

- KeyCDN - lightning fast CDN mirror
- Duplicator - dupe anything
- Mailchimp - all you need for mailing list and sign up
- Popup Maker - cookie enabled modals
- Regen Thumbs - Regenerate all image sizes
- SVG support - add SVG graphics in a jiffy
- Yoast - SEO for wordpress
- Woocommerce - commerce
- Smart Slider - another versitile Slider
- WP CSV Importer - for importing CSV files

If any other plugins are add to the project please send details to  [technical@web123.com.au](mailto:technical@web123.com.au)


# Build Tools


Inside the Web123-child folder is a preconfigured gulp task runner that complies SCSS to the style.css file.

Please make sure to add any uniquely styled pages or sections as partials in the `sass` folder.

```
web123-child
├── build
│   ├── js
│   │   └── app.js
│   ├── sass
│   │   ├── _breakpoints.scss
│   │   ├── _components.scss
│   │   ├── _footer.scss
│   │   ├── _global.scss
│   │   ├── _header.scss
│   │   ├── _holding-page.scss
│   │   ├── _home-page.scss
│   │   ├── _variables.scss
│   │   ├── _wp-child-style-banner.scss
│   │   └── style.scss
│   └── tempCopy.html
├── functions.php
├── gulpfile.js
├── package.json
├── screenshot.png
├── style.css
├── style.css.map
└── yarn.lock
```

If you are using ES6 feel free to amend the gulp.js file accordingly.

#### Yarn

This project uses YARN package manager not NPM - please make sure you use `yarn install` to set up the project locally. For more on yarn [https://yarnpkg.com/en/](https://yarnpkg.com/en/)




For assistance please contact [technical@web123.com.au](mailto:technical@web123.com.au)
