function updatemenu(){
	var _ = jQuery,css='',container_id = '.noonav.' + _('.noonav').attr('id')+' ',container = '.noonav.' + _('.noonav').attr('id');
	
		css += container_id + '.noo-megamenu.animate .mega > .mega-dropdown-menu{';
		css += 'transition-duration:'+_('input#animation_duration').val()+'ms;';
		css += '-moz-transition-duration:'+_('input#animation_duration').val()+'ms;';
		css += '-webkit-transition-duration:'+_('input#animation_duration').val()+'ms;';
		
		css += 'transition-delay:'+_('input#animation_delay').val()+'ms;';
		css += '-moz-transition-delay:'+_('input#animation_delay').val()+'ms;';
		css += '-webkit-transition-delay:'+_('input#animation_delay').val()+'ms;';
		css +='}';
		
		//menu bar
		css += container_id + '.noo-megamenu {';
		css += 'border-top-left-radius:'+ _('input#menu_bar_radius_top_left').val() +'px;';
		css += '-moz-border-top-left-radius:'+ _('input#menu_bar_radius_top_left').val() +'px;';
		css += '-o-border-top-left-radius:'+ _('input#menu_bar_radius_top_left').val() + 'px;';
		css += 'border-top-right-radius:'+ _('input#menu_bar_radius_top_right').val() +'px;';
		css += '-moz-border-top-right-radius:'+ _('input#menu_bar_radius_top_right').val() +'px;';
		css += '-o-border-top-right-radius:'+ _('input#menu_bar_radius_top_right').val() + 'px;';
		css += 'border-bottom-right-radius:'+ _('input#menu_bar_radius_bottom_right').val() +'px;';
		css += '-moz-border-bottom-right-radius:'+ _('input#menu_bar_radius_bottom_right').val() +'px;';
		css += '-o-border-bottom-right-radius:'+ _('input#menu_bar_radius_bottom_right').val() + 'px;';
		css += 'border-bottom-left-radius:'+ _('input#menu_bar_radius_bottom_left').val() +'px;';
		css += '-moz-border-bottom-left-radius:'+ _('input#menu_bar_radius_bottom_left').val() +'px;';
		css += '-o-border-bottom-left-radius:'+ _('input#menu_bar_radius_bottom_left').val() + 'px;';
		
		if(_('input#menu_bar_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+'), to('+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#menu_bar_solid_background_color').data('rgbastring')+';'
		}
		
		_('#menu_bar_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+_this.val() +':'+_('input#menu_bar_border_width').val()+'px '+_('select#menu_bar_border_style').val()+' '+_('input#menu_bar_boder_color').val() +';';
			}
		});
	
	
		css +='padding-right:'+_('input#menu_bar_padding_right').val()+'px;';
		css +='padding-left:'+_('input#menu_bar_padding_left').val()+'px;';
		css +='margin-right:'+_('input#menu_bar_margin_right').val()+'px;';
		css +='margin-left:'+_('input#menu_bar_margin_left').val()+'px;';
		
		css +='}';
		
		css += container + '.noosticky > div{';
		css +='width:'+_('input#sticky_wrap').val()+';';
		css +='}';
		
		//menu bar
		css += container_id + '.noo-megamenu.horizontal {';
		css +='height:'+_('input#menu_bar_height').val()+'px;';
		css +='}';
		
		css += container+'.noo-nav-mb1{';
		css +='box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px '+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';\
		-webkit-box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px'+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';\
		-moz-box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px'+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';';

		css +='}';
		//container
		css += container_id +  '{';
		if(_('input#menu_bar_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+'), to('+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#menu_bar_solid_background_color').data('rgbastring')+';'
		}
		css += 'border-top-left-radius:'+ _('input#menu_bar_radius_top_left').val() +'px;';
		css += '-moz-border-top-left-radius:'+ _('input#menu_bar_radius_top_left').val() +'px;';
		css += '-o-border-top-left-radius:'+ _('input#menu_bar_radius_top_left').val() + 'px;';
		css += 'border-top-right-radius:'+ _('input#menu_bar_radius_top_right').val() +'px;';
		css += '-moz-border-top-right-radius:'+ _('input#menu_bar_radius_top_right').val() +'px;';
		css += '-o-border-top-right-radius:'+ _('input#menu_bar_radius_top_right').val() + 'px;';
		css += 'border-bottom-right-radius:'+ _('input#menu_bar_radius_bottom_right').val() +'px;';
		css += '-moz-border-bottom-right-radius:'+ _('input#menu_bar_radius_bottom_right').val() +'px;';
		css += '-o-border-bottom-right-radius:'+ _('input#menu_bar_radius_bottom_right').val() + 'px;';
		css += 'border-bottom-left-radius:'+ _('input#menu_bar_radius_bottom_left').val() +'px;';
		css += '-moz-border-bottom-left-radius:'+ _('input#menu_bar_radius_bottom_left').val() +'px;';
		css += '-o-border-bottom-left-radius:'+ _('input#menu_bar_radius_bottom_left').val() + 'px;';
		css +='}';
		
		//logo
		css +=container_id + '.noo-menu-logo{';
		css +='height:'+_('input#top_menu_height').val()+'px;';
		css +='line-height:'+_('input#top_menu_height').val()+'px;';
		css +='}';
		//search
		css +=container_id +  '.noo-menu-search,'+ container_id + ' .noo-menu-cart{';
		css +='height:'+_('input#top_menu_height').val()+'px;';
		css +='line-height:'+_('input#top_menu_height').val()+'px;';
		css +='color: '+_('input#top_menu_color').val()+';';
		css +='}';
		
		css += container_id +  '.noo-menu-search-show-hover .field:focus,'+container_id +  '.noo-menu-search-show-alway .field{';
		css +='background-color:'+_('input#search_background_color').data('rgbastring') +';';
		css +='}';
		css +=  container_id +  ' .noo-menu-search i{';
		css +='color:'+_('input#search_icon_color').val() +';';
		css +='}';
		css +=container_id +  '.noo-menu-cart a{';
		css +='color: '+_('input#top_menu_color').val()+';';
		css +='}';
		
		//Top menu
		css +=container_id + '.noo-nav > li > a{';
		css +='height:'+_('input#top_menu_height').val()+'px;';
		css +='line-height:'+_('input#top_menu_height').val()+'px;';
		
		css +='font-weight: '+_('select#top_menu_font_weight').val()+';';
		css +='font-family: '+_('select#top_menu_font').val()+';';
		css +='font-style: '+_('select#top_menu_font_style').val()+';';
		css +='font-size: '+_('input#top_menu_font_size').val()+'px;';
		css +='color: '+_('input#top_menu_color').val()+';';
		
		
		if(_('input#top_menu_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#top_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#top_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#top_menu_gradient_background_start_color').data('rgbastring')+'), to('+_('input#top_menu_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#top_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#top_menu_solid_background_color').data('rgbastring')+';'
		}
		
		if(_('input#top_menu_font_uppercase').is(':checked')){
			css +='text-transform: uppercase;';
		}
		_('#top_menu_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +':'+_('input#top_menu_border_width').val()+'px '+_('select#top_menu_border_style').val()+' '+_('input#top_menu_boder_color').val() +';';
			}
		});
		css +='text-shadow: '+_('input#top_menu_font_h_shadow').val()+'px '+_('input#top_menu_font_v_shadow').val()+'px '+_('input#top_menu_font_b_shadow').val()+'px '+_('input#top_menu_font_shadow_color').data('rgbastring')+';';
		css +='box-shadow: '+_('input#top_menu_h_shadow').val()+'px '+_('input#top_menu_v_shadow').val()+'px '+_('input#top_menu_b_shadow').val()+'px '+_('input#top_menu_s_shadow').val()+'px '+_('input#top_menu_shadow_color').data('rgbastring')+''+(_('input#top_menu_shadow_inset').is(':checked') ? ' inset' : '')+';\
				-webkit-box-shadow: '+_('input#top_menu_h_shadow').val()+'px '+_('input#top_menu_v_shadow').val()+'px '+_('input#top_menu_b_shadow').val()+'px '+_('input#top_menu_s_shadow').val()+'px '+_('input#top_menu_shadow_color').data('rgbastring')+''+(_('input#top_menu_shadow_inset').is(':checked') ? ' inset' : '')+';\
				-moz-box-shadow: '+_('input#top_menu_h_shadow').val()+'px '+_('input#top_menu_v_shadow').val()+'px '+_('input#top_menu_b_shadow').val()+'px '+_('input#top_menu_s_shadow').val()+'px '+_('input#top_menu_shadow_color').data('rgbastring')+''+(_('input#top_menu_shadow_inset').is(':checked') ? ' inset' : '')+';';
		css +='padding-right:'+_('input#top_menu_padding_right').val()+'px;';
		css +='padding-left:'+_('input#top_menu_padding_left').val()+'px;';
		css +='margin-top:'+_('input#top_menu_vertical_position').val()+'px;';
		css +='margin-right:'+_('input#top_menu_margin_right').val()+'px;';
		css +='margin-left:'+_('input#top_menu_margin_left').val()+'px;';
		
		css += 'border-top-left-radius:'+ _('input#top_menu_radius_top_left').val() +'px;';
		css += '-moz-border-top-left-radius:'+ _('input#top_menu_radius_top_left').val() +'px;';
		css += '-o-border-top-left-radius:'+ _('input#top_menu_radius_top_left').val() + 'px;';
		css += 'border-top-right-radius:'+ _('input#top_menu_radius_top_right').val() +'px;';
		css += '-moz-border-top-right-radius:'+ _('input#top_menu_radius_top_right').val() +'px;';
		css += '-o-border-top-right-radius:'+ _('input#top_menu_radius_top_right').val() + 'px;';
		css += 'border-bottom-right-radius:'+ _('input#top_menu_radius_bottom_right').val() +'px;';
		css += '-moz-border-bottom-right-radius:'+ _('input#top_menu_radius_bottom_right').val() +'px;';
		css += '-o-border-bottom-right-radius:'+ _('input#top_menu_radius_bottom_right').val() + 'px;';
		css += 'border-bottom-left-radius:'+ _('input#top_menu_radius_bottom_left').val() +'px;';
		css += '-moz-border-bottom-left-radius:'+ _('input#top_menu_radius_bottom_left').val() +'px;';
		css += '-o-border-bottom-left-radius:'+ _('input#top_menu_radius_bottom_left').val() + 'px;';
		
		css +='}';
		
		css +=container_id + '.noo-nav > li > a .noo-icon{';
		css +='color: '+_('input#top_menu_icon_color').val()+';';
		css +='font-size: '+_('input#top_menu_icon_size').val()+'px;';
		css +='}';
		
		css += container_id +' .navbar-brand{';
		css +='color: '+_('input#top_menu_color').val()+';';
		css +='}';
		
		//caret
		css += container_id + '.noo-megamenu .caret{';
	//	css +='border-bottom-color: '+_('input#top_menu_color').val()+';';
		css +='border-top-color: '+_('input#top_menu_color').val()+';';
		css +='}';
		
		//top menu hover
		css +=container_id + '.noo-nav > li:hover > a,'+container_id  + '.noo-nav > li.open > a,'+container_id + '.noo-nav > .current-menu-item > a,'+container_id + ' .noo-nav > .current-menu-ancestor > a,'+container_id + ' .noo-nav > .current_page_item > a,'+container_id + ' .noo-nav > .current_page_ancestor > a{';
		
		_('#top_menu_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +'-color:' +_('input#top_menu_hover_boder_color').val() +';';
			}
		});
		
		css +='color: '+_('input#top_menu_hover_color').val()+';';
		if(_('input#top_menu_hover_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+'), to('+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#top_menu_hover_solid_background_color').data('rgbastring')+';'
		}
		
		css +='text-shadow: '+_('input#top_menu_hover_font_h_shadow').val()+'px '+_('input#top_menu_hover_font_v_shadow').val()+'px '+_('input#top_menu_hover_font_b_shadow').val()+'px '+_('input#top_menu_hover_font_shadow_color').data('rgbastring')+';';
		css +='}';
		
		css +=container_id + '.noo-nav > li:hover > a .noo-icon,'+container_id  + '.noo-nav > li.open > a .noo-icon,'+container_id + '.noo-nav > .current-menu-item > a .noo-icon,'+container_id + ' .noo-nav > .current-menu-ancestor > a .noo-icon,'+container_id + ' .noo-nav > .current_page_item > a .noo-icon,'+container_id + ' .noo-nav > .current_page_ancestor > a .noo-icon{';
		css +='color: '+_('input#top_menu_hover_icon_color').val()+';';
		css +='}';
		
		css +=container_id +'.noo-nav li.dropdown.open .caret,'+container_id +' .noo-nav li.dropdown.open.active .caret,'+container_id +' .noo-nav li.dropdown.open a:hover .caret,'+container_id +'.noo-nav .dropdown-toggle:hover .caret,'+container_id +'.noo-nav > li:hover > a > .caret,'+container_id +'.noo-nav > .current-menu-item > a > .caret,'+container_id +' .noo-nav > .current-menu-ancestor > a > .caret,'+container_id +' .noo-nav > .current_page_item > a > .caret,'+container_id +' .noo-nav > .current_page_ancestor > a > .caret{';
		css +='border-top-color: '+_('input#top_menu_hover_color').val()+';';
		css +='border-bottom-color: '+_('input#top_menu_hover_color').val()+';';
		css +='}';
		

		css += '@media (max-width: 767px) {'+container_id +' .noo-nav > li.mega > a:after{border-color:'+_('input#top_menu_color').val()+' rgba(0, 0, 0, 0)} '+container_id +' .noo-nav > li.mega:hover > a:after,'+container_id +' .noo-nav > li.mega.open > a:after,'+container_id +' .noo-nav > li.mega.current-menu-item > a:after,'+container_id +' .noo-nav > li.mega.current_page_item > a:after,'+container_id +' .noo-nav > li.mega.current_page_ancestor > a:after{border-color:'+_('input#top_menu_hover_color').val()+' rgba(0, 0, 0, 0)}}';

		//sub menu
		css +=container_id +'.noo-megamenu .dropdown-menu,'+container_id +'.noo-megamenu .dropdown-submenu > .dropdown-menu{';

		css += 'border-top-left-radius:'+ _('input#sub_menu_radius_top_left').val() +'px;';
		css += '-moz-border-top-left-radius:'+ _('input#sub_menu_radius_top_left').val() +'px;';
		css += '-o-border-top-left-radius:'+ _('input#sub_menu_radius_top_left').val() + 'px;';
		css += 'border-top-right-radius:'+ _('input#sub_menu_radius_top_right').val() +'px;';
		css += '-moz-border-top-right-radius:'+ _('input#sub_menu_radius_top_right').val() +'px;';
		css += '-o-border-top-right-radius:'+ _('input#sub_menu_radius_top_right').val() + 'px;';
		css += 'border-bottom-right-radius:'+ _('input#sub_menu_radius_bottom_right').val() +'px;';
		css += '-moz-border-bottom-right-radius:'+ _('input#sub_menu_radius_bottom_right').val() +'px;';
		css += '-o-border-bottom-right-radius:'+ _('input#sub_menu_radius_bottom_right').val() + 'px;';
		css += 'border-bottom-left-radius:'+ _('input#sub_menu_radius_bottom_left').val() +'px;';
		css += '-moz-border-bottom-left-radius:'+ _('input#sub_menu_radius_bottom_left').val() +'px;';
		css += '-o-border-bottom-left-radius:'+ _('input#sub_menu_radius_bottom_left').val() + 'px;';
		
		
		if(_('input#sub_menu_solid_background').is(':checked')){
			css += 'background-color:'+ _('input#sub_menu_solid_background_color').data('rgbastring')+';'
		}else{
			css +=' background: linear-gradient(top,  '+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+'), to('+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+');';
		}
		
		_('#sub_menu_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +':'+_('input#sub_menu_border_width').val()+'px '+_('select#sub_menu_border_style').val()+' '+_('input#sub_menu_border_color').val() +';';
			}
		});
		
		css +='box-shadow: '+_('input#sub_menu_box_h_shadow').val()+'px '+_('input#sub_menu_box_v_shadow').val()+'px '+_('input#sub_menu_box_b_shadow').val()+'px '+_('input#sub_menu_box_s_shadow').val()+'px '+_('input#sub_menu_box_shadow_color').data('rgbastring')+''+(_('input#sub_menu_box_shadow_inset').is(':checked') ? ' inset' : '')+';\
			-webkit-box-shadow: '+_('input#sub_menu_box_h_shadow').val()+'px '+_('input#sub_menu_box_v_shadow').val()+'px '+_('input#sub_menu_box_b_shadow').val()+'px '+_('input#sub_menu_box_s_shadow').val()+'px '+_('input#sub_menu_box_shadow_color').data('rgbastring')+''+(_('input#sub_menu_box_shadow_inset').is(':checked') ? ' inset' : '')+';\
			-moz-box-shadow: '+_('input#sub_menu_box_h_shadow').val()+'px '+_('input#sub_menu_box_v_shadow').val()+'px '+_('input#sub_menu_box_b_shadow').val()+'px '+_('input#sub_menu_box_s_shadow').val()+'px '+_('input#sub_menu_box_shadow_color').data('rgbastring')+''+(_('input#sub_menu_box_shadow_inset').is(':checked') ? ' inset' : '')+';';
		css +='padding: '+_('input#sub_menu_box_padding_top').val()+'px '+_('input#sub_menu_box_padding_right').val()+'px '+_('input#sub_menu_box_padding_bottom').val()+'px '+_('input#sub_menu_box_padding_left').val()+'px;';
	
		css +='}'; 
		
		css += container_id + '.noo-megamenu .dropdown-menu .mega-nav > li > a{';
		css +='font-weight: '+_('select#sub_menu_font_weight').val()+'px;';
		css +='font-family: '+_('select#sub_menu_font').val()+';';
		css +='font-style: '+_('select#sub_menu_font_style').val()+';';
		css +='font-size: '+_('input#sub_menu_font_size').val()+'px;';
		css +='color: '+_('input#sub_menu_color').val()+';';
		if(_('input#sub_menu_font_uppercase').is(':checked')){
			css +='text-transform: uppercase;';
		}
		css +='text-shadow: '+_('input#sub_menu_font_h_shadow').val()+'px '+_('input#sub_menu_font_v_shadow').val()+'px '+_('input#sub_menu_font_b_shadow').val()+'px '+_('input#sub_menu_font_shadow_color').data('rgbastring')+';';
		css +='padding: '+_('input#sub_menu_item_padding_top').val()+'px '+_('input#sub_menu_item_padding_right').val()+'px '+_('input#sub_menu_item_padding_bottom').val()+'px '+_('input#sub_menu_item_padding_left').val()+'px;';
		_('#sub_menu_item_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +':'+_('input#sub_menu_item_border_width').val()+'px '+_('select#sub_menu_item_border_style').val()+' '+_('input#sub_menu_item_border_color').val() +';';
			}
		});
		css += '}';
		
		css +=container_id + '.noo-megamenu .dropdown-menu .mega-nav > li > a .noo-icon{';
		css +='color: '+_('input#sub_menu_icon_color').val()+';';
		css +='font-size: '+_('input#sub_menu_icon_size').val()+'px;';
		css +='}';
		
		//sub menu hover
		css += container_id +'.noo-megamenu .dropdown-menu .mega-nav > li:hover > a:not(.mega-group-title){';
		
		_('#sub_menu_item_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +'-color:' +_('input#sub_menu_item_hover_boder_color').val() +';';
			}
		});
		
		css +='color: '+_('input#sub_menu_hover_color').val()+';';
		
		if(_('input#sub_menu_hover_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+'), to('+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#sub_menu_hover_solid_background_color').data('rgbastring')+';'
		}
		css +='text-shadow: '+_('input#sub_menu_hover_font_h_shadow').val()+'px '+_('input#sub_menu_hover_font_v_shadow').val()+'px '+_('input#sub_menu_hover_font_b_shadow').val()+'px '+_('input#sub_menu_hover_font_shadow_color').data('rgbastring')+';';
		
		css += '}';
		
		css += container_id +'.noo-megamenu .dropdown-menu .mega-nav > li:hover > a .noo-icon{';
		css +='color: '+_('input#sub_menu_hover_icon_color').val()+';';
		css +='}';

		css += '@media (max-width: 767px) {'+container_id +'.noo-megamenu .dropdown-menu .mega-nav > li > a:after{border-color:'+_('input#sub_menu_color').val()+' rgba(0, 0, 0, 0)} '+container_id +' .noo-megamenu .dropdown-submenu > a:after,'+container_id +' .noo-megamenu .mega-group > a:after{border-color:'+_('input#top_menu_hover_color').val()+' rgba(0, 0, 0, 0)}}';

	_("#noo-menu-custom-css").remove();
	_('<style id="noo-menu-custom-css" type="text/css">'+css+'</style>').appendTo('head');
	return css;
}