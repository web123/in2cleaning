getBottomPosition = function (f, g) {
    var c = 0;
    try {
        if (typeof (g) == "undefined" || typeof (g.bottom) == "undefined") {
            var d = (f).parent();
            if (d != null && typeof (d) != "undefined") {
                c = d.offset().top + d.outerHeight(true)
            }
        } else {
            if (typeof (g.bottom) == "string") {
                var a = g.bottom;
                if (a.indexOf("#") == -1) {
                    a = "#" + a
                }
                c = jQuery(a).offset().top
            } else {
                c = g.bottom
            }
        }
    } catch (b) {}
    return c
};
doPositionSet = function (h, j, f) {
    var d = getBottomPosition(h, j);
    var g = 0;
    if (j.top) {
        g = j.top
    }
    if (d > 0) {
        var a = jQuery(window).scrollTop() + jQuery(window).height() - d;
        if (jQuery(window).scrollTop() > h.T0 - g) {   	
            jQuery(h).css("position", "fixed");
            jQuery(h).css("width", h.W0-2);
            jQuery(h).css("height", h.H0);
            jQuery(h).css("left", h.L0);
            jQuery(h).css("z-index", 999);
            if (a > 0) {
                if (jQuery(window).scrollTop() + h.H0 > d) {
                    jQuery(h).css("top", g);
                    jQuery(h).css("bottom", Math.floor(a))
                } else {
                    jQuery(h).css("top", g + "px")
                }
            } else {
                jQuery(h).css("top", g + "px")
            }
        } else {
            jQuery(h).css("position", "static");
        }
    }
    if (f > 0) {
        for (var c = 0; c < f; c++) {
            window.setTimeout(function () {
                doPositionSet(h, j, 0)
            }, 100)
        }
    }
};
doDimesionSet = function (a) {
    a.T0 = jQuery(a).offset().top;
    a.L0 = jQuery(a).offset().left;
    a.H0 = jQuery(a).height();
    a.W0 = jQuery(a).width()
};
doMakeFixed = function (d, c) {
    var b = jQuery(d)
    doDimesionSet(b);
    var a = getBottomPosition(b, c);
    jQuery(window).bind("resize", function (e) {
        jQuery(b).each(function () {
            jQuery(b).css("position", "static");
            doDimesionSet(b)
        })
    });
    jQuery(window).bind("scroll", function (e) {
        doPositionSet(b, c, 5)
    })
};