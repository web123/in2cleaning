/* scroll-up-bar v0.2.0 (https://github.com/eduardomb/scroll-up-bar) */
(function($) {
  'use strict';

  $.fn.noomenuscrolluphidden = function() {
    var $window = $(window),
        $document = $(document),
        $topbar = this,
        topbarHeight = $topbar.outerHeight(),
        lastY = $window.scrollTop(), // Use last Y to detect scroll direction.
        iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent),
        timeout;

    // iOS can't handle momentum scroll properly (See discussion on
    // http://stackoverflow.com/questions/2863547).
    if (!iOS) {
      $window.scroll(function() {
        var y = $window.scrollTop(),
            offsetBottom = $topbar.offset().top + topbarHeight,
            barIsHidden = offsetBottom <= y && y >= topbarHeight;

        // Ignore elastic scrolling.
        if (y < 0 || y > ($document.height() - $window.height())) {
          return;
        }
        if (y < lastY || y < topbarHeight) { // Scrolling up
        	if($topbar.hasClass('noo-sticking'))
        		$topbar.addClass('noo-menu-hidden');
        } else if (y > lastY) { // Scrolling down
        	if($topbar.hasClass('noo-menu-hidden')){
          	  $topbar.removeClass('noo-menu-hidden')
            }
        }

        lastY = y;
      });
    } else { // Fallback simplified behaviour for iOS.
      $(document).on('touchstart', function () {
        lastY = $window.scrollTop();
      });

      $(document).on('touchend', function () {
        var y = $window.scrollTop();

        if (y < lastY || y < topbarHeight) { // Scrolling up
        	if($topbar.hasClass('noo-sticking'))
        		$topbar.slideDown();
        } else if (y > lastY) { // Scrolling down
          $topbar.slideUp();
        }

        lastY = y;
      });
    }

    return this;
  };
})(jQuery);
