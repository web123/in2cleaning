var AdminMegamenu = window.AdminMegamenu || {};

var actions = window.actions || {};

var noo_state_flag = window.noo_state_flag || false;

var currentSelected = window.currentSelected || null;
var noo_menu_load_frame = window.noo_menu_load_frame || {};

!function ($) {
	var megamenu, nav_items, nav_subs, nav_cols, nav_all;
	
	$.fn.megamenuAdmin = function (options) {
		var defaultOptions = {
				
		};
		var options = $.extend(defaultOptions, options);
		megamenu = $(this).find('.noo-megamenu');
		nav_items = megamenu.find('ul[class*="level"]>li>:first-child');
		nav_subs = megamenu.find('.noo-nav-child');
		nav_cols = megamenu.find('[class*="noo-span"]');
		
		nav_all = nav_items.add(nav_subs).add(nav_cols);
		// hide sub 
		nav_items.each (function () {			
			var a = $(this),
				liitem = a.closest('li');
			if (liitem.data ('hidesub') == 1) {
				var sub = liitem.find('.noo-nav-child:first');
				// check if have menu-items in sub
				sub.css('display','none');
				a.removeClass ('dropdown-toggle').data('toggle', '');
				liitem.removeClass('dropdown dropdown-submenu mega');
			}
		});
		// hide toolbox
		hide_toolbox(true);
		
		//switch listener
		switchListener();
		
		// bind events for all selectable elements
		bindEvents (nav_all);

		// unbind all events for toolbox actions & inputs
		$('.toolbox-action, .toolbox-toggle, .toolbox-input').unbind ("focus blur click change keydown");

		// stop popup event when click in toolbox area
		$('.noo-admin-mm-row').click (function(event) {
			event.stopPropagation();
			// return false;
		});
		// deselect when click outside menu
		//$(document.body).click (function(event) {
			//hide_toolbox (true);
			//event.stopPropagation();
		//});

		// bind event for action
		
		$('.toolbox-action').click (function(event) {
			var action = $(this).data ('action');

			if (action) {
				actions.datas = $(this).data();
				actions[action] ();
			}
			
			//save flag
			noo_state_flag = true;
			
			event.stopPropagation();
			return false;
		});
		$('.toolbox-toggle').change (function(event) {
			var action = $(this).data ('action');
			if (action) {
				actions.datas = $(this).data();
				actions[action] ();
			}
			
			//save flag
			noo_state_flag = true;
			
			event.stopPropagation();
			return false;
		});
		// ignore events
		$('.toolbox-input').bind ('focus blur click', function(event) {
			event.stopPropagation();
			return false;
		});
		$('.toolbox-input').bind ('keydown', function(event) {
			if (event.keyCode == '13') {
				apply_toolbox (this);
				event.preventDefault();
			}
		});

		$('.toolbox-input').change (function(event) {
			apply_toolbox (this);
			event.stopPropagation();
			return false;
		});

		return this;
	};

	// Actions
	//var actions = {};
	actions.data = {};
	
	actions.listWidgets = function(element){
		var target = $('#nooWidgetControlModal');
		
		$.post(NooMegamenuOptions.ajax_url,{
			action: 'noo_menu_list_widgets',
			security: NooMegamenuOptions.get_list_widgets_security
		},function(res){
			target.find('.modal-body').empty().html(res.list_widgets);
			target.modal('show');
			
			$('#nooWidgetControlModal a.widget-title').each(function(){
				var $this = $(this)
				$this.click(function(event){
					$.post(NooMegamenuOptions.ajax_url,{
						action:'noo_menu_add_widget',
						security: NooMegamenuOptions.add_widget_security,
						widget_id: $this.data('widget-id') 
					},function(res){
						if(res.widget_form){
							var n = $(res.widget_form).find( 'input.multi_number' ).val();
							var id = $(res.widget_form).find('input.widget-id').val();
							target.find('#nooWidgetControlModalLabel').text($this.data('widget-title'));
							target.find('.modal-body').empty().html(res.widget_form.replace( /<[^<>]+>/g, function(m) {
									return m.replace( /__i__|%i%/g, n );
								})
							);
							
							$('#nooWidgetControlModalSubmit').show().one('click',function(event){
								var spiner = $(this).parent().find('.spinner');
								spiner.show();
								
								var data = $('form#nooWidgetForm').serialize();
								
								$.post(NooMegamenuOptions.ajax_url,data,function(res){	
									
									var data = {
											action: 'widgets-order',
											savewidgets: $('#savewidgets').val(),
											sidebars:[]
										};
										data['sidebars[' + NooMegamenuOptions.sidebar_id + ']'] = $('#sidebars_noo_megamenu_list_widgets').val();
										$('.noo-menu-widget-order').each(function(){
											var _this = $(this);
											data['sidebars[' + _this.data('sidebar-id') + ']'] = _this.val();
										});
										$.post( NooMegamenuOptions.ajax_url,data, function(res) {
											//get widget list options
											if(res && res > 0){
												
												$.post( NooMegamenuOptions.ajax_url,{
													action: 'noo_menu_get_option_widget',
													security: NooMegamenuOptions.get_option_widget,
													seleced: id
												},function(res){
													
													spiner.hide();
													
													$('.toolcol-widget').html(res).trigger("liszt:updated");
													var input = $('.toolcol-widget');
														value = $('.toolcol-widget').val();
													
													//console.log(res);
													
													if (currentSelected.find ('ul[class*="level"]').length == 0) {
														// get module content
														if (value) {
															var moduleTitle = $(input).find('option:selected').text();
//															console.log(moduleTitle);
															var moduleTemplate = '<div class="widget-title-template"><i class="icon-code"></i>'+moduleTitle+'</div>';
															currentSelected.find('.mega-inner').html(moduleTemplate)

														} else {
															currentSelected.find('.mega-inner').html('');
														}
														currentSelected.data (input.data('name'),value);
													}
													target.modal('hide');
												});
											}
										});
										event.stopPropagation();
										event.preventDefault();
										return false;
									});
									
									
								});
								
						}
					},'json');
					event.preventDefault();
					event.stopPropagation();
					
					return false;
				});
			});
			
		},'json');
		
	}
	
	actions.toggleSub = function () {
		if (!currentSelected) return ;
		var liitem = currentSelected.closest('li'),
		sub = liitem.find ('.noo-nav-child:first');
		if (liitem.data('group')) return; // not allow do with group
		if (sub.length == 0 || sub.css('display') == 'none') {
			// add sub
			if (sub.length == 0) {
				sub = $('<div class="noo-nav-child dropdown-menu mega-dropdown-menu"><div class="noo-row"><div class="noo-span12" data-width="12"><div class="mega-inner"></div></div></div></div>').appendTo(liitem);
				bindEvents (sub.find ('[class*="noo-span"]'));
				liitem.addClass ('mega');
			} else {
				// sub.attr('style', '');
				sub.css('display','');
				liitem.data('hidesub', 0);
			}
			liitem.data('group', 0);
			currentSelected.addClass ('dropdown-toggle').data('toggle', 'dropdown');
			liitem.addClass(liitem.data('level') == 1 ? 'dropdown' : 'dropdown-submenu');
			bindEvents(sub);
		} else {
			unbindEvents(sub);
			// check if have menu-items in sub
			if (liitem.find('ul.level'+liitem.data('level')).length > 0) {
				sub.css('display','none');
				liitem.data('hidesub', 1);
			} else {
				// just remove it
				sub.remove();
			}
			liitem.data('group', 0);
			currentSelected.removeClass ('dropdown-toggle').data('toggle', '');
			liitem.removeClass('dropdown dropdown-submenu mega');
		}
		// update toolbox status
		update_toolbox ();
	}

	actions.toggleGroup = function () {
		if (!currentSelected) return ;
		var liitem = currentSelected.parent(),
			sub = liitem.find ('.noo-nav-child:first');
		if (liitem.data('level') == 1) return; // ignore for top level
		if (liitem.data('group')) {
			liitem.data('group', 0);
			liitem.removeClass('mega-group').addClass('dropdown-submenu');
			currentSelected.addClass ('dropdown-toggle').data('toggle', 'dropdown');
			sub.removeClass ('mega-group-ct').addClass ('dropdown-menu mega-dropdown-menu');
			sub.css('width', sub.data('width'));
			rebindEvents(sub);
		} else {
			currentSelected.removeClass ('dropdown-toggle').data('toggle', '');
			liitem.data('group', 1);
			liitem.removeClass('dropdown-submenu').addClass('mega-group');
			sub.removeClass ('dropdown-menu mega-dropdown-menu').addClass ('mega-group-ct');
			sub.css('width', '');
			rebindEvents(sub);
		}
		// update toolbox status
		update_toolbox ();
	}

	actions.moveItemsLeft = function () {
		if (!currentSelected) return ;
		var $item = currentSelected.closest('li'),
		$liparent = $item.parent().closest('li'),
		level = $liparent.data('level'),
		$col = $item.closest ('[class*="noo-span"]'),
		$items = $col.find ('ul:first > li'),
		itemidx = $items.index ($item),
		$moveitems = $items.slice (0, itemidx+1),
		itemleft = $items.length - $moveitems.length,
		$rows = $col.parent().parent().children ('[class*="row"]'),
		$cols = $rows.children('[class*="noo-span"]').filter (function(){return !$(this).data('position')}),
		colidx = $cols.index ($col);
		if (!$liparent.length) return ; // need make this is mega first

		if (colidx == 0) {
			// add new col
			var oldSelected = currentSelected;
			currentSelected = $col;
			// add column to first
			actions.datas.addfirst = true;
			actions.addColumn ();
			$cols = $rows.children('[class*="noo-span"]').filter (function(){return !$(this).data('position')});
			currentSelected = oldSelected;
			colidx++;
		}
		// move content to right col
		var $tocol = $($cols[colidx-1]);
		var $ul = $tocol.find('ul:first');
		if (!$ul.length) {
			$ul = $('<ul class="mega-nav level'+level+'">').appendTo ($tocol.children('.mega-inner'));
		}
		$moveitems.appendTo($ul);
		if (itemleft == 0) {
			$col.find('ul:first').remove();
		}
		// update toolbox status
		update_toolbox ();
	}

	actions.moveItemsRight = function () {
		if (!currentSelected) return ;
		var $item = currentSelected.closest('li'),
		$liparent = $item.parent().closest('li'),
		level = $liparent.data('level'),
		$col = $item.closest ('[class*="noo-span"]'),
		$items = $col.find ('ul:first > li'),
		itemidx = $items.index ($item),
		$moveitems = $items.slice (itemidx),
		itemleft = $items.length - $moveitems.length,
		$rows = $col.parent().parent().children ('[class*="row"]'),
		$cols = $rows.children('[class*="noo-span"]').filter (function(){return !$(this).data('position')}),
		colidx = $cols.index ($col);
		if (!$liparent.length) return ; // need make this is mega first

		if (colidx == $cols.length - 1) {
			// add new col
			var oldSelected = currentSelected;
			currentSelected = $col;
			actions.datas.addfirst = false;
			actions.addColumn ();
			$cols = $rows.children('[class*="noo-span"]').filter (function(){return !$(this).data('position')});
			currentSelected = oldSelected;
		}
		// move content to right col
		var $tocol = $($cols[colidx+1]);
		var $ul = $tocol.find('ul:first');
		if (!$ul.length) {
			$ul = $('<ul class="mega-nav level'+level+'">').appendTo ($tocol.children('.mega-inner'));
		}
		$moveitems.prependTo($ul);
		if (itemleft == 0) {
			$col.find('ul:first').remove();
		}
		// update toolbox status
		show_toolbox (currentSelected);
	}

	actions.addRow = function () {
		if (!currentSelected) return ;
		var $row = $('<div class="noo-row"><div class="noo-span12"><div class="mega-inner"></div></div></div>').appendTo(currentSelected.find('[class*="row"]:first').parent()),
		$col = $row.children();
		// bind event
		bindEvents ($col);
		currentSelected = null;
		// switch selected to new column
		show_toolbox ($col);
	}

	actions.alignment = function () {
		var liitem = currentSelected.closest ('li');
		liitem.removeClass ('mega-align-left mega-align-center mega-align-right mega-align-justify').addClass ('mega-align-'+actions.datas.align);
		if (actions.datas.align == 'justify') {
			currentSelected.addClass('noo-span12');
			currentSelected.css('width', '');
		} else {
			currentSelected.removeClass('noo-span12');
			if (currentSelected.data('width')) currentSelected.css('width', currentSelected.data('width'));
		}
		liitem.data('alignsub', actions.datas.align);
		update_toolbox ();
	}

	actions.addColumn = function () {
		if (!currentSelected) return ;
		var $cols = currentSelected.parent().children('[class*="noo-span"]'),
			colcount = $cols.length + 1,
			colwidths = defaultColumnsWidth (colcount);
			
		// add new column  
		var $col = $('<div><div class="mega-inner"></div></div>');
		if (actions.datas.addfirst) 
			$col.prependTo (currentSelected.parent());
		else {
			$col.insertAfter (currentSelected);
		}
		$cols = $cols.add ($col);
		// bind event
		bindEvents ($col);
		// update widthf
		$cols.each (function (i) {
			$(this).removeClass ('noo-span'+$(this).data('width')).addClass('noo-span'+colwidths[i]).data('width', colwidths[i]);
		});
		// switch selected to new column
		
		//console.log(colcount);
		var li_parent = currentSelected.closest('.noo-nav-item');
		if(!li_parent.hasClass('mega-align-justify')){
			var nav_child = currentSelected.closest('.noo-nav-child');
			if(!nav_child.hasClass('mega-group-ct'))
				nav_child.width((colcount*200+'px')).data('width',(colcount*200+'px'));
		}
		
		show_toolbox ($col);
	}

	actions.removeColumn = function () {
		if (!currentSelected){
			return;
		}

		var $col = currentSelected,
			$row = $col.parent(),
			$rows = $row.parent().children ('[class*="row"]'),
			$allcols = $rows.children('[class*="noo-span"]'),
			$allmenucols = $allcols.filter (function(){return !$(this).data('position')}),
			$haspos = $allcols.filter (function(){return $(this).data('position')}).length,
			$cols = $row.children('[class*="noo-span"]'),
			colcount = $cols.length - 1,
			colwidths = defaultColumnsWidth (colcount),
			type_menu = $col.data ('position') ? false : true;

		if ((type_menu && ((!$haspos && $allmenucols.length == 1) || ($haspos && $allmenucols.length == 0))) 
			|| $allcols.length == 1) {
			// if this is the only one column left
			return;
		}

		// remove column  
		// check and move content to other column        
		if (type_menu) {
			var colidx = $allmenucols.index($col),
				tocol = colidx == 0 ? $allmenucols[1] : $allmenucols[colidx-1];

			$col.find ('ul:first > li').appendTo ($(tocol).find('ul:first'));
		} 

		var colidx = $allcols.index($col),
			nextActiveCol = colidx == 0 ? $allcols[1] : $allcols[colidx-1];
		
		if (colcount < 1) {
			$row.remove();
		} else {            
			$cols = $cols.not ($col);
			// update width
			$cols.each (function (i) {
				$(this).removeClass ('noo-span'+$(this).data('width')).addClass('noo-span'+colwidths[i]).data('width', colwidths[i]);
			});
			// remove col
			$col.remove();
		}

		show_toolbox ($(nextActiveCol));
		//console.log(colcount);
		var li_parent = currentSelected.closest('.noo-nav-item');
		if(!li_parent.hasClass('mega-align-justify')){
			var nav_child = currentSelected.closest('.noo-nav-child');
			if(!nav_child.hasClass('mega-group-ct')){
				nav_child.width(( ((colcount*200) ) +'px')).data('width',( ((colcount*200) ) +'px'));
			}
		}
		
	}

	actions.hideWhenCollapse = function () {		
		if (!currentSelected) return ;
		var type = toolbox_type ();
		if (type == 'sub') {
			var liitem = currentSelected.closest('li');
			if (liitem.data('hidewcol')) {
				liitem.data('hidewcol', 0);
				liitem.removeClass ('sub-hidden-collapse');
			} else {
				liitem.data('hidewcol', 1);
				liitem.addClass ('sub-hidden-collapse');
			}			
		} else if (type == 'col') {
			if (currentSelected.data('hidewcol')) {
				currentSelected.data('hidewcol', 0);
				currentSelected.removeClass ('hidden-collapse');
			} else {
				currentSelected.data('hidewcol', 1);
				currentSelected.addClass ('hidden-collapse');			
			}			
		}
		update_toolbox ();
	}

	actions.saveConfig = function (e) {
		var config = {},
		items = megamenu.find('ul[class*="level"] > li');
		items.each (function(){
			var $this = $(this),
			id = 'item-'+$this.data('id'),
			item = {};
			if ($this.hasClass ('mega')) {
				var $sub = $this.find ('.noo-nav-child:first');
				item['sub'] = {};
				
				for (var d in $sub.data()) {
					if (d != 'id' && d != 'level' && $sub.data(d))
						item['sub'][d] = $sub.data(d);
				}
				// build row
				var $rows = $sub.find('[class*="row"]:first').parent().children('[class*="row"]'),
				rows = [],
				i = 0;

				$rows.each (function () {
					var row = [],arow = [],
					$cols = $(this).children('[class*="noo-span"]'),
					j = 0;
					$cols.each (function(){
						var li = $(this).find('ul[class*="level"] > li:first'),
							ali = li.parent().children('li'),
							col = {};
						if (li.length ) {
							col['item'] = li.data('id');
						} else if ($(this).data('widget')) {
							col['widget'] = $(this).data('widget');
						} else {
							col['item'] = -1;
						}
						
						for (var d in $(this).data()) {
							if (d != 'id' && d != 'level' && d != 'widget' && $(this).data(d)){
								col[d] = $(this).data(d);
							}
						}
						row[j++] = col;
					});
					rows[i++] = row;
				});
				item['sub']['rows'] = rows;
			}

			for (var d in $this.data()) {
				if (d != 'id' && d != 'level' && d != 'sortableItem' && $this.data(d)) {
					if (d == 'caption') {
						item[d] = $this.data(d).replace(/</g, "[lt]").replace(/>/g, "[gt]");
					}
					else 
						item[d] = $this.data(d);
				}
			}

			if (Object.keys(item).length) config[id] = item;
		});

		var menu_config = $('#noo_magemenu_config'),
			curconfig = null,
			menu_style = $('#noo_magemenu_style');

		try {
			curconfig = menu_config.val() ? $.parseJSON(menu_config.val()) : {};
		} catch(e){
			curconfig = {};
		}

		if($.isArray(curconfig) && curconfig.length == 0){
			curconfig = {};
		}
		curconfig = config;
		var jsonConfig = JSON.stringify(curconfig);
		//console.log(jsonConfig);
		menu_config.val(jsonConfig);
		
		menu_style.val(updatemenu());
		
		
	}

	toolbox_type = function () {
		return currentSelected.hasClass ('noo-nav-child') ? 'sub' : (currentSelected[0].tagName == 'DIV' ? 'col':'item');
	}

	hide_toolbox = function (show_intro) {
		$('#noo-admin-preview-intro').hide();
		
		$('#noo-admin-mm-tb .admin-toolbox').hide();
		currentSelected = null;
		if (megamenu && megamenu.data('nav_all')) megamenu.data('nav_all').removeClass ('selected');
		megamenu.find ('li').removeClass ('open');
		if (show_intro) {
			$('#noo-admin-mm-intro').show();
		} else {
			$('#noo-admin-mm-intro').hide();
		}
	}

	show_toolbox = function (selected) {
		$('#noo-admin-preview-intro').hide();
		
		hide_toolbox (false);
		if (selected) currentSelected = selected;
		// remove class open for other
		megamenu.find ('ul[class*="level"] > li').each (function(){
			if (!$(this).has (currentSelected).length > 0) $(this).removeClass ('open');
			else $(this).addClass ('open');
		});            

		// set selected
		megamenu.data('nav_all').removeClass ('selected');
		currentSelected.addClass ('selected');		
		var type = toolbox_type ();
		$('#noo-admin-mm-tool' + type).show();
		update_toolbox (type);

		$('#noo-admin-mm-tb').show();
	}

	update_toolbox = function (type) {
		if (!type) type = toolbox_type ();
		// remove all disabled status
		$('#noo-admin-mm-tb .disabled').removeClass('disabled');
		$('#noo-admin-mm-tb .active').removeClass('active');

		switch (type) {
			case 'item':
				// value for toggle
				var liitem = currentSelected.closest('li'),
					liparent = liitem.parent().closest('li'),
					sub = liitem.find ('.noo-nav-child:first');
					
				$('.toolitem-exclass').attr('value', liitem.data ('class') || '');
				$('.toolitem-xicon').attr('value', liitem.data ('xicon') || '');
				$('.toolitem-caption').attr('value', liitem.data ('caption') || '');
				// toggle Display
				var toggle = $('.toolitem-display');
				toggle.find('label').removeClass('active');
				if(liitem.data('display') == 1){
					update_toggle (toggle,1);
				} else {
					// sub enabled
					update_toggle (toggle,0	);
				}	
				
				// toggle Submenu
				var toggle = $('.toolitem-sub');
				toggle.find('label').removeClass('active');
				if (liitem.data('group')) {
					// disable the toggle
					$('.toolitem-sub').addClass ('disabled');
				} else if (sub.length == 0 || sub.css('display') == 'none') {
					// sub disabled
					update_toggle (toggle, 0);
				} else {
					// sub enabled
					update_toggle (toggle, 1);
				}				
				
				// toggle Group
				var toggle = $('.toolitem-group');
				toggle.find('label').removeClass('active');
				if (liitem.data('level') == 1 || sub.length == 0 || liitem.data('hidesub') == 1) {
					// disable the toggle
					$('.toolitem-group').addClass ('disabled');
				} else if (liitem.data('group')) {
					// Group off
					update_toggle (toggle, 1);
				} else {
					// Group on
					update_toggle (toggle, 0);				
				}

				// move left/right column action: disabled if this item is not in a mega submenu
				if (!liparent.length || !liparent.hasClass('mega')) {
					$('.toolitem-moveleft, .toolitem-moveright').addClass ('disabled');
				}

				break;

			case 'sub':
				var liitem = currentSelected.closest('li');
				$('.toolsub-exclass').attr('value', currentSelected.data ('class') || '');

				if (liitem.data('group')) {
					$('.toolsub-width').attr('value', '').addClass ('disabled');
					// disable alignment
					$('.toolsub-alignment').addClass ('disabled');
				} else {
					$('.toolsub-width').attr('value', currentSelected.data ('width') || '');
					// if not top level, allow align-left & right only
					if (liitem.data('level') > 1) {
						$('.toolsub-align-center').addClass ('disabled');
						$('.toolsub-align-justify').addClass ('disabled');
					} 

					// active align button
					if (liitem.data('alignsub')) {
						$('.toolsub-align-'+liitem.data('alignsub')).addClass ('active');
						if (liitem.data('alignsub') == 'justify') {
							$('.toolsub-width').addClass ('disabled');
						}
					}					
				}

				// toggle hidewhencollapse
				var toggle = $('.toolsub-hidewhencollapse');
				toggle.find('label').removeClass('active');
				if (liitem.data('hidewcol')) {
					// toggle enable
					update_toggle (toggle, 1);
				} else {
					// toggle disable
					update_toggle (toggle, 0);
				}	

				break;

			case 'col':
				$('.toolcol-exclass').attr('value', currentSelected.data ('class') || '');
				//$('.toolcol-position').attr('value', currentSelected.data ('position') || '');
				//$('.toolcol-width').attr('value', currentSelected.data ('width') || '');
				$('.toolcol-widget').val (currentSelected.data ('widget') || '').trigger("liszt:updated");
				$('.toolcol-width').val (currentSelected.data ('width') || '').trigger("liszt:updated");
				/* enable/disable module chosen */
				if (currentSelected.find ('.mega-nav').length > 0) {
					$('.toolcol-widget').parent().addClass('disabled');
				}
				// disable choose width if signle column
				if (currentSelected.parent().children().length == 0) {
					$('.toolcol-width').parent().addClass ('disabled');
				}

				// toggle hidewhencollapse
				var toggle = $('.toolcol-hidewhencollapse');
				toggle.find('label').removeClass('active');
				if (currentSelected.data('hidewcol')) {
					// toggle enable
					update_toggle (toggle, 1);
				} else {
					// toggle disable
					update_toggle (toggle, 0);
				}	
					
				break;
		}
	}

	update_toggle = function (toggle, val) {
		$input = toggle.find('input[value="'+val+'"]');
		
		$input.attr('checked', 'checked');
		$input.trigger ('update');
	}

	apply_toolbox = function (input) {
		var name = $(input).data ('name'), 
		value = input.value,
		type = toolbox_type ();
		switch (name) {
			case 'width':
				if (type == 'sub') {
					currentSelected.width(value);
				}
				if (type == 'col') {
					currentSelected.removeClass('noo-span'+currentSelected.data(name)).addClass ('noo-span'+value);
				}
				currentSelected.data (name, value);
				break;

			case 'class':
				if (type == 'item') {
					var item = currentSelected.closest('li');
				} else {
					var item = currentSelected;
				}
				item.removeClass(item.data(name) || '').addClass (value);
				item.data (name, value);
				break;

			case 'xicon':
				if (type == 'item') {
					currentSelected.closest('li').data (name, value);
					currentSelected.find('i').remove();
					if (value) currentSelected.prepend($('<i class="'+value+'"></i>'));
				}
				break;

			case 'caption':
				if (type == 'item') {
					currentSelected.closest('li').data (name, value);
					currentSelected.find('span.mega-caption').remove();
					if (value) currentSelected.append($('<span class="mega-caption">'+value+'</span>'));
				}
				break;

			case 'widget':
				// replace content if this is not menu-items type
				if (currentSelected.find ('ul[class*="level"]').length == 0) {
					// get module content
					if (value) {
						var moduleTitle = $(input).find('option:selected').text();
//						console.log(moduleTitle);
						var moduleTemplate = '<div class="widget-title-template"><i class="icon-code"></i>'+moduleTitle+'</div>';
						currentSelected.find('.mega-inner').html(moduleTemplate)

					} else {
						currentSelected.find('.mega-inner').html('');
					}
					currentSelected.data (name, value);
				}
				break;
		}
	}

	defaultColumnsWidth = function (count) {
		if (count < 1) return null;
		var total = 12,
		min = Math.floor(total / count),
		widths = [];
		for(var i=0;i<count;i++) {
			widths[i] = min;
		}
		widths[count - 1] = total - min*(count-1);
		return widths;
	}
	
	bindEvents = function (els) {
		if (megamenu.data('nav_all')) 
			megamenu.data('nav_all', megamenu.data('nav_all').add(els));
		else
			megamenu.data('nav_all', els);

		els.click (function(event){
			$('.noo-switch span.off').trigger('click');
			show_toolbox ($(this));
			
			event.stopPropagation(); 
			
			return false;
		});
	}

	switchListener = function(){
		$('#noo_menu_item_options').click(function(e){
			hide_toolbox(true);
			if($('#noo-admin-megamenu').hasClass('noo-menu-item-options')){
				
			}else{
				$('#noo-admin-megamenu').addClass('noo-menu-item-options');
				$('#noo-menu-css').remove();
				$('#noo-menu-default-css').remove();
				$('#noo-menu-custom-css').remove();
			}
			e.preventDefault();
			e.stopPropagation();
		
		});
		
		$('#noo_menu_style_options').click(function(e){
			hide_toolbox(true);
			//window.scrollTo(0,$('#noo_menu_css_generator').offset().top - 200);
			$('#noo-admin-mm-intro').hide();
			$('#noo-admin-preview-intro').show();
			
			if($('#noo-admin-megamenu').hasClass('noo-menu-item-options')){
				$('<link id="noo-menu-css" media="all" type="text/css" href="'+NooMegamenuOptions.noo_menu_css+'" rel="stylesheet">').appendTo('head');
				$('<link id="noo-menu-default-css" media="all" type="text/css" href="'+NooMegamenuOptions.noo_menu_default_css+'" rel="stylesheet">').appendTo('head');
				
				updatemenu();
				
				$('#noo-admin-megamenu').removeClass('noo-menu-item-options');
			}
			e.preventDefault();
			e.stopPropagation();
		});
		
	    $('.noo-switch').find('span').click(function(){
	    	$('.noo-switch').find('.active').removeClass("active");
	    	$(this).addClass('active');
	    	var trigger = $(this).data('trigger');
	    	$('#'+trigger).trigger('click');
	    	
	    });
	}
	
	unbindEvents = function (els) {
		megamenu.data('nav_all', megamenu.data('nav_all').not(els));
		els.unbind('mouseover').unbind('mouseout').unbind('click');
	}

	rebindEvents = function (els) {
		unbindEvents(els);
		bindEvents(els);
	}
	
	$.fn.fieldSerializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {	
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
}(jQuery);

!function($){
	
	$.extend(AdminMegamenu, {
		prepare :function(){
			$('#noo_menu_use_search').change(function(){
				var val = $(this).val();
				if(val =='yes'){
					$('#noomenuSearch').show();
				}else{
					$('#noomenuSearch').hide();
				}
			});
			
			$('#noo_menu_use_logo').change(function(){
				var val = $(this).val();
				if(val =='yes'){
					$('#noomenulogoUpload').show();
				}else{
					$('#noomenulogoUpload').hide();
				}
			});
			
			$('select#orientation').change(function(){
				var val = $(this).val();
				var horizontal_dropdown_box = $('#horizontal_dropdown_box');
				var vertical_dropdown_box = $('#vertical_dropdown_box');
				horizontal_dropdown_box.hide();
				vertical_dropdown_box.hide();
				if(val =='horizontal'){
					horizontal_dropdown_box.show();
				}else{
					vertical_dropdown_box.show();
				}
			});
			
			// Only show the "remove image" button when needed
			 if ( ! $('#noo_menu_logo_thumbnail_id').val() )
				 $('.remove_image_button').hide();

			// Uploading files
			var file_frame;

			$(document).on( 'click', '.upload_image_button', function( event ){

				event.preventDefault();

				// If the media frame already exists, reopen it.
				if ( file_frame ) {
					file_frame.open();
					return;
				}

				// Create the media frame.
				file_frame = wp.media.frames.downloadable_file = wp.media({
					title: 'Choose an image',
					button: {
						text: 'Use image',
					},
					multiple: false
				});

				// When an image is selected, run a callback.
				file_frame.on( 'select', function() {
					attachment = file_frame.state().get('selection').first().toJSON();

					$('#noo_menu_logo_thumbnail_id').val( attachment.id );
					$('#noo_menu_logo_thumbnail img').attr('src', attachment.url );
					$('.remove_image_button').show();
				});

				// Finally, open the modal.
				file_frame.open();
			});

			$(document).on( 'click', '.remove_image_button', function( event ){
				$('#noo_menu_logo_thumbnail img').attr('src',NooMegamenuOptions.plugin_url + 'assets/images/noo-logo.png');
				$('#noo_menu_logo_thumbnail_id').val('');
				$('.remove_image_button').hide();
				return false;
			});
			
			var noomegamenu = $('#noo-admin-mm-container').megamenuAdmin().find(':input').removeAttr('name');
			
			var get_preset_style,delete_preset_style,delete_preset_style_event;
			
			delete_preset_style_event = function(delete_event){
				$('.noo-menu-del-preset-style').click(function(e){
					if(window.confirm("You are about to delete style. \n 'Cancel' to stop, 'OK' to delete.")){
						$('.noo-menu-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
						
						delete_event($(this).data('id'));
					}
					e.preventDefault();
					e.stopPropagation();
				});
			};
			
			delete_preset_style = function(id){
				$.post(NooMegamenuOptions.ajax_url,{
					 action:'noo_menu_delete_style',
					 style_id:id
				 },function(r){
					if(r){
						get_preset_style();
					}
				 });
			};
			get_preset_style = function(){
				$.post(NooMegamenuOptions.ajax_url,{
					 action:'noo_menu_get_style' 
				 },function(r){
					 $('#noo_menu_perset').html(r);
					 $('.noo-menu-frame').unblock();
					 delete_preset_style_event(delete_preset_style);
					 $('.noo-menu-apply-preset-style').click(function(e){
						$(this).closest('.toolbox-actions-group').find('.spinner').show();
						noo_menu_load_frame($(this).data('id'));
						e.stopPropagation();
						return;
					});
				 });
			};
			
			delete_preset_style_event(delete_preset_style);
			
			
			$('#noo_menu_save_preset_style_button').click(function(e){
				 e.preventDefault();
				 var name = window.prompt("Please enter name","");
				 if((typeof name === "string") && name.length ){
					 $('.noo-menu-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
						
					var data = $('#noo_menu_css_generator').find("select,textarea, input").serialize(),a;
						a = {
							action:'noo_menu_save_style',
							style_name : name,
							menu:$('#nooMenuForm').find('input#menu').val()
						}
						data += '&' + $.param(a);
					 $.post(NooMegamenuOptions.ajax_url,data,
					 function(r){
						 if(r){
							 get_preset_style();
						 }
					 });
				 }
			});
			
		    $('#noTabs a[data-toggle="tab"]').click(function (e) {
		        e.preventDefault();
		        if($(this).hasClass('noo-custom-style-tab')){
		        	$('#noo_menu_style_options').trigger('click');
		        	
		        	$('.noo-switch').find('span[data-trigger="noo_menu_style_options"]').click();
		        
		        }
		        $(this).tab('show');
		    })
		    
		    //postpox
		    postboxes.add_postbox_toggles(NooMegamenuOptions.plugin_page);
			
			$(".slider").each(function(){
				var $this = $(this),
					run = $this.data('run') || 0 ;
				if($this.attr('id') == 'top_menu_height'){
					$this.data('max',$('input#menu_bar_height').val());
				}
				$this[0].slide = null;
				$this.removeAttr('slide').slider({ 
					range:  "min",
					value:  $this.next('input').val() || 0,
					min: $this.data('min') || 0,
					max: $this.data('max') || 100,
					slide: function(event, ui) {
						$this.parent().find('span#'+$this.attr('id')).text(ui.value);
						$this.next('input').val(ui.value).attr('value',ui.value);
						if(run >= 0){
							updatemenu();
						}
					},
					stop: function(event,ui){
						
						noo_state_flag = true;
						
						if($this.attr('id') == 'menu_bar_height'){
							if(parseInt($('input#menu_bar_height').val()) < parseInt($('input#top_menu_height').val()) ){
								$('div#top_menu_height').slider('value',$('input#menu_bar_height').val());
								$('div#top_menu_height').parent().find('span#'+ $('div#top_menu_height').attr('id')).text($('input#menu_bar_height').val());
								$('div#top_menu_height').next('input').val($('input#menu_bar_height').val()).attr('value',$('input#menu_bar_height').val());
							}

							
							
							$('div#top_menu_height').slider('option','max',$('input#menu_bar_height').val());
							
						}
						
						
						if($('input#menu_bar_height').val() <= $('input#top_menu_height').val()){
							$('div#top_menu_vertical_position').parent().find('span#'+ $('div#top_menu_vertical_position').attr('id')).text(0);
							$('div#top_menu_vertical_position').next('input').val(0).attr('value',0);
							$('div#top_menu_vertical_position').slider('value',0);
							$('div#top_menu_vertical_position').slider('disable');
						}else{
							$('div#top_menu_vertical_position').slider('enable');
							var max_ver_pos = parseInt($('input#menu_bar_height').val()) - parseInt($('input#top_menu_height').val());
							if($('div#top_menu_vertical_position').next('input').val() > max_ver_pos){
								$('div#top_menu_vertical_position').parent().find('span#'+ $('div#top_menu_vertical_position').attr('id')).text(max_ver_pos);
								$('div#top_menu_vertical_position').next('input').val(max_ver_pos).attr('value',max_ver_pos);
								$('div#top_menu_vertical_position').slider('value',max_ver_pos);
							}
							$('div#top_menu_vertical_position').slider('option','max', max_ver_pos);
						}
						if(run >= 0){
							updatemenu();
						}
					}
				});
			});	
			$(".color").each(function(){
				var $this = $(this);
				$this.minicolors({
					control: $this.attr('data-control') || '',
					defaultValue: $this.attr('data-defaultValue') || '',
					inline: $this.attr('data-inline') === 'true',
					letterCase: $this.attr('data-letterCase') || 'lowercase',
					opacity: $this.attr('data-opacity'),
					position: $this.attr('data-position') || 'bottom left',
					change: function(hex, opacity) {
						if( !hex ) return;
						if( opacity ) {
							var id = $this.attr('id');
							$('#'+id+'_opacity').val(opacity).attr('value',opacity);
						}
						noo_state_flag = true;
					},
					theme: 'bootstrap'
				});
			});
			
			$('select.google-font').on('change',function(){
				$("#noo-menu-google-font"+id).remove();
				if($(this).val() != 'inherit'){
					var id = $(this).attr('id');
					var url = $(this).find('option:selected').data('url');
					$(this).next('input').val(url).attr('value',url);
					var rel = '<link id="noo-menu-google-font'+id+'" media="all" type="text/css" href="//fonts.googleapis.com/css?'+url+'" rel="stylesheet">';
					$(rel).appendTo('head');
				}
			});
			

			$('select.google-font').each(function(){
				var id = $(this).attr('id');
				var url = $(this).find('option:selected').data('url');
				$(this).next('input').val(url).attr('value',url);
				var rel = '<link id="noo-menu-google-font'+id+'" media="all" type="text/css" href="//fonts.googleapis.com/css?'+url+'" rel="stylesheet">';
				$("#noo-menu-google-font"+id).remove();
				$(rel).appendTo('head');
			});
			
			
			$('select#animation').on('change',function(){
				$('.noo-megamenu').removeClass("animate fading slide zoom elastic").addClass('animate').addClass($(this).val());
			});
			
			
			$('.action-group').find('.btn').click(function(){
				$(this).closest('.action-group').find('.btn').removeClass('active');
				$(this).addClass('active');
			});
			
			//font awesome dialog
			$('#nooFontAwesomeDialog').click(function(e){
				initIconsDialog();
				iconsDialogShow();
				e.preventDefault();
				e.stopPropagation();
			});
			
		},
		initRadioGroup: function(){
			//return;
			var noomenu = $('.noo-admin-megamenu');

			noomenu.find('.radio.btn-group label').addClass('btn');
			noomenu.find('.btn-group label').unbind('click').click(function() {
				var label = $(this),
					input = $('#' + label.attr('for'));
				
				if (!input.prop('checked')){
					label.closest('.btn-group')
						.find('label')
						.removeClass('active');

					label.addClass('active');
					input.prop('checked', true).trigger('change');
				}
			});

			noomenu.on('update', 'input[type=radio]', function(){
				if(this.checked){
					$(this).closest('.btn-group')
						.find('label').removeClass('active')
						.filter('[for="' + this.id + '"]').addClass('active');
				}
			});

			noomenu.find('.btn-group input[checked=checked]').each(function(){
				$('label[for=' + $(this).attr('id') + ']').addClass('active');
			});
			
			
		}
	});

	$(document).ready(function(){
		
		var nav_menus_frame = $('#nav-menus-frame'),
			menu = nav_menus_frame.find('input#menu').val(),
			nav_menu_save = $('.menu-save'),
			apply_preset_event,
			switch_button;
		
		noo_menu_load_frame = function(style_id,enable){
			style_id = style_id || 0;
			enable = enable || 0;
			if(!style_id){
				$('#noo-menu-custom-css').remove();
				$('.noo-menu-frame').remove();
			}
			var data = $('form#update-nav-menu #post-body input,form#update-nav-menu #post-body select,form#update-nav-menu #post-body textarea').serialize(),a;
			a = {
				action: 'noo_menu_get_page',
				security: NooMegamenuOptions.get_page_security,
				menu: menu,
				style_id: style_id
			};
			data += '&'+ $.param(a);
			$.post(NooMegamenuOptions.ajax_url,data,function(r){
				var html = $(r);
				if(enable){
					html.find('#noo-menu-enable').val(1).prop('value',1);
				}
				if(style_id){
					$('#noo-menu-custom-css').remove();
					$('.noo-menu-frame').remove();
				}
				nav_menus_frame.after(html);
				
				if(!enable){
					window.scrollTo(0,html.offset().top);
				}
				
				AdminMegamenu.prepare();
				
				apply_preset_event();
				
				AdminMegamenu.initRadioGroup();
			    
				window.setTimeout(function(){
			        doMakeFixed($('.noo-megamenu.horizontal').closest(".noonav"), {bottom:'#wpfooter',top:32 });
			    },100);
				
				if(!style_id){
					if($('.noo-menu-frame').hasClass('show')){
						$('.noo-menu-frame').removeClass('show');
					}else{
						$('.noo-menu-frame').addClass('show');
					}
					$('.switch_button_mega_builder').parent().find('.spinner').hide();
					$('#nav-menus-frame').toggle();
				}else{
					$('.toolbox-actions-group').find('.spinner').hide();
					$('.noo-menu-frame').addClass('show');
				}
				
				//Noo switch
				$('.noo-menu-switch').click(function(event){
					if(noo_state_flag){
						$('#idSwitchDialog').modal('show');
						$('#idSwitchDialog .modal-footer').find('button').click(function(e){
							var value = $(this).data('value');
							if(value == '1'){
								$('#idSwitchDialog').modal('hide');
								$('.noo-menu-save-change').click();
							}else if(value == '-1'){
								$('#idSwitchDialog').modal('hide');
							}else{
								$('#idSwitchDialog').modal('hide');
								$('.noo-menu-frame').toggleClass('show');
								$('#nav-menus-frame').toggle();
							}
							e.preventDefault();
							e.stopPropagation();
						});
						
						return;
					}
					$('.noo-menu-frame').toggleClass('show');
					$('#nav-menus-frame').toggle();
					event.stopPropagation();
					return;
				});
				
				
				$('.noo-menu-save-change').click(function(event){
					$('.noo-menu-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
					
					$('.toolbox-saveConfig').trigger('click');
					window.onbeforeunload = null;
					var data = $('#nooMenuForm').serialize(),a,items = [];
					
					$('.noo-megamenu').find('ul[class*="level"] > li').each(function(){
						var item = {};
							item.id = $(this).data('id');
						if($(this).data('level') > 1){
							var nav_child = $(this).closest('.noo-nav-child');
							item.parent = nav_child.parent().data('id');
						}else{
							item.parent = 0;
						}
						items.push(item);
					});
					
					a = {
							action: 'noo_menu_update_option',
							security: NooMegamenuOptions.update_option_security,
							menu: html.find('input#menu').val()
//							,reorder: JSON.stringify(items)
					};
					data += '&' + $.param(a);
					
					$.post(NooMegamenuOptions.ajax_url,data,function(r){
						$('form#update-nav-menu').submit();
					});
					
					event.stopPropagation();
					return;
				});
				
				if(style_id){
					$('.noo-switch span.on').trigger('click');
//					$('#noo_menu_style_options').trigger('click');
				}
				
				//delete settings
				$('#noo-menu-clear-config').click(function(e){
					if(window.confirm(NooMegamenuOptions.deleteMenuSetting)){
						$('.noo-menu-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
						
						window.onbeforeunload = null;
						$.post(NooMegamenuOptions.ajax_url,{
							action:'noo_menu_delete_option',
							security: NooMegamenuOptions.delete_option_security,
							menu: html.find('input#menu').val()
						},function(r){
							noo_menu_load_frame(-1);
						});
					}
					e.preventDefault();
					e.stopPropagation();
					
					return;
				});
				
				//disable
				$('#noo-menu-disable-nav').click(function(e){
					if(window.confirm(NooMegamenuOptions.disableMegamenu)){
						$('.noo-menu-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
						
						window.onbeforeunload = null;
						$('#noo-menu-enable').val(-1).prop('value',-1);
						$('.noo-menu-save-change').click();
						
					}

					e.preventDefault();
					e.stopPropagation();
					return;
				});
			});
		};
		
		apply_preset_event = function(){
			$('.noo-menu-apply-preset-style').click(function(e){
				$(this).closest('.toolbox-actions-group').find('.spinner').show();
				noo_menu_load_frame($(this).data('id'));
				e.stopPropagation();
				return;
			});
		};
		
		if(menu > 0){
			
			$('#nav-menus-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
			$.post(NooMegamenuOptions.ajax_url,{action:'noo_menu_is_enable',menu:menu},function(r){
				var switch_button = $('<span class="spinner" style="float:left"></span><button '+ (r > 0 ? '' : 'data-enable="1"') +' id="switch_button_mega_builder" class="button button-large button-primary switch_button_mega_builder" type="button">'+(r > 0 ? NooMegamenuOptions.navSwitchText : NooMegamenuOptions.navEnableText )+'</button>');
				
				$('#nav-menus-frame').unblock();
				
				$('.menu-save').parent().prepend(switch_button);

				$('.switch_button_mega_builder').click(function(event){
					var spinner = $(this).parent().find('.spinner');
					spinner.show();
					
					noo_menu_load_frame(0,$(this).data('enable'));
					
					
					event.preventDefault();
					event.stopPropagation();
					return;
				});
				if(r > 0){
					$('.menu-save').click(function(e){
						
						$('#nav-menus-frame').block({ message: null, overlayCSS: { background: '#fff url(' + NooMegamenuOptions.plugin_url + 'assets/images/ajax-loader.gif) no-repeat center', opacity: 0.6 } });
						
						
						window.onbeforeunload = null;
						wpNavMenu.eventOnClickMenuSave();
						$('.noo-menu-frame').remove();
						
						var data = $('form#update-nav-menu #post-body input,form#update-nav-menu #post-body select,form#update-nav-menu #post-body textarea').serialize(),a;
						a = {
							action: 'noo_menu_get_page',
							security: NooMegamenuOptions.get_page_security,
							menu: menu
						};
						data += '&'+ $.param(a);
						$.post(NooMegamenuOptions.ajax_url,data,function(r){
							var html = $(r);
							nav_menus_frame.after(html);

							AdminMegamenu.prepare();
							AdminMegamenu.initRadioGroup();
							$('.toolbox-saveConfig').trigger('click');
							
							var data = $('#nooMenuForm').serialize(),a;
							a = {
									action: 'noo_menu_update_option',
									security: NooMegamenuOptions.update_option_security,
									menu: html.find('input#menu').val()
							};
							data += '&' + $.param(a);
							
							$.post(NooMegamenuOptions.ajax_url,data,function(r){
								$('form#update-nav-menu').submit();
							});
						});
						
						
						e.preventDefault();
						e.stopPropagation();
						return false;
					});
				}
				
			});
		}
	});
	
}(window.jQuery);
