<?php
class Noo_Select_Walker_Nav_Menu extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0 , $args = array() ) {
		$indent = str_repeat( "\t", $depth );
	}
	
	function end_lvl(&$output, $depth = 0 , $args = array() ) {
		$indent = str_repeat("\t", $depth);
	}
	
	function start_el( &$output, $item, $depth = 0, $args = array(), $current_object_id = 0 ){
		
		global $responsiveMenuSelect;
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$dashes = ( $depth ) ? str_repeat( '&ndash; ', $depth ) : '';	//"&ndash; "

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		if( ( $item->url == '#' || $item->url == '' ) ){
			return;
		}


		//$attributes = ! empty( $item->url )        ? ' value="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes = ' value="'   . esc_attr( $item->url        ) .'"';
		
		if(strpos( $class_names , 'current-menu-item' ) > 0 ){
			$attributes.= ' selected="selected"';
		}
		
		$output .= $indent . '<option ' . $id . $attributes . '>';

		$item_output = $args->before;
		$item_output .= $dashes . $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= $args->after;

		$output.= str_replace( '%', '%%', $item_output );

		$output.= "</option>\n";
	}
	
	function end_el(&$output, $item, $depth = 0 , $args = array() ) {
		return;		
	}
}