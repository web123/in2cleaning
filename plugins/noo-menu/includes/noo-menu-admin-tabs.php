<div id="poststuff">
<div id="noo_menu_css_generator" class="metabox-holder clearfix">
	<ul class="tabs nav nav-tabs clearfix" id="noTabs">
		<li class="active"><a class="noo-custom-style-tab" data-toggle="tab" href="#general"><?php echo __('Functionality',NOO_MENU) ?></a></li>
		<li><a class="noo-custom-style-tab noo-adv-ctr noo-menu-bar-style" data-toggle="tab" href="#tab1"><?php echo __('Menu Bar',NOO_MENU) ?></a></li>
		<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab2"> <?php echo __('Top Menu',NOO_MENU) ?></a></li>
		<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab3"> <?php echo __('Top Menu Hover',NOO_MENU) ?></a></li>
		<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab4"> <?php echo __('Sub Menu',NOO_MENU) ?></a></li>
		<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab5"> <?php echo __('Sub Menu Hover',NOO_MENU) ?></a></li>
		<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab6"> <?php echo __('Custom Style',NOO_MENU) ?></a></li>
	</ul>
	<div id="tabsContent" class="tab-content">
		<div class="tab-pane fade in active" id="general">
		<br>
			<div class="noo-row">
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_menu_orientation_div" class="postbox <?php echo postbox_classes('noo_menu_orientation_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Menu Orientation',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<label for="orientation"><?php echo __('Orientation',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('orientation')?>" id="orientation" class="form-control">
									<option <?php if(noo_menu_get_option('orientation','horizontal') == 'horizontal'):?> selected="selected"<?php endif; ?> value="horizontal"><?php echo __('Horizontal', NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('orientation','horizontal') == 'vertical'):?> selected="selected"<?php endif; ?>value="vertical"><?php echo __('Vertical',NOO_MENU) ?></option>
								</select>
							</p>
							<p id="horizontal_dropdown_box" style="display:<?php if(noo_menu_get_option('orientation','horizontal') == 'horizontal'):?>block<?php else:?>none<?php endif?>">
								<label for="horizontal_dropdown"><?php echo __('Submenu Direction',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('horizontal_dropdown')?>" id="dropdown" class="form-control">
									<option <?php if(noo_menu_get_option('horizontal_dropdown','down') == 'up'): ?> selected="selected"<?php endif;?>  value="up"><?php echo __('Horizontal Up',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('horizontal_dropdown','down') == 'down'): ?> selected="selected"<?php endif;?> value="down"><?php echo __('Horizontal Down',NOO_MENU) ?></option>
								</select>
							</p>
							<p style="display: <?php if(noo_menu_get_option('orientation','horizontal') == 'vertical'):?>block<?php else:?>none<?php endif?>;" id="vertical_dropdown_box">
								<label for="dropdown"><?php echo __('Submenu Direction',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('vertical_dropdown')?>" id="dropdown" class="form-control">
									<option <?php if(noo_menu_get_option('vertical_dropdown','down') == 'ltr'): ?> selected="selected"<?php endif;?> value="ltr"><?php echo __('Vertical Left To Right',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('vertical_dropdown','down') == 'rtl'): ?> selected="selected"<?php endif;?> value="rtl"><?php echo __('Vertical Right to Left',NOO_MENU) ?></option>
								</select>
							</p>
						</div>
					</div>
					<div id="noo_menu_branding_div" class="postbox <?php echo postbox_classes('noo_menu_branding_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Branding',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
							<label for="noo_menu_use_logo"><?php echo __('Logo (on the left)',NOO_MENU) ?></label>
							<select name="<?php echo noo_menu_get_option_name('logo')?>" id="noo_menu_use_logo"  class="form-control">
								<option <?php if(noo_menu_get_option('logo','yes') == 'yes'): ?> selected="selected"<?php endif;?> value="yes"><?php echo __('Yes',NOO_MENU) ?></option>
								<option <?php if(noo_menu_get_option('logo','yes') == 'no'): ?> selected="selected"<?php endif;?> value="no"><?php echo __('No',NOO_MENU) ?></option>
							</select>
							</p>
							
							<div style="clear: both;display:<?php if(noo_menu_get_option('logo','yes') == 'yes'): ?>block<?php else: ?>none<?php endif?>" id="noomenulogoUpload">
								<label style="clear: both; width: 100%; float: left; margin-bottom: 5px;"><?php echo __('Logo',NOO_MENU) ?></label>
								<div style="float:left;margin-right:10px;" id="noo_menu_logo_thumbnail">
									<?php 
									$logo = NOO_MENU_URL.'assets/images/noo-logo.png';
									if(noo_menu_get_option('menu_logo',0) > 0){
										$logo = wp_get_attachment_thumb_url(noo_menu_get_option('menu_logo',0));
									}
									?>
									<img width="60px" src="<?php echo $logo ?>">
								</div>
								<div style="line-height:60px;">
									<input type="hidden" name="<?php echo noo_menu_get_option_name('menu_logo')?>" id="noo_menu_logo_thumbnail_id" value="<?php echo noo_menu_get_option('menu_logo',0)?>">
									<button class="upload_image_button button" type="button"><?php echo __('Upload/Add image',NOO_MENU) ?></button>
									<button class="remove_image_button button" type="button" style="display: none;"><?php echo __('Remove image',NOO_MENU) ?></button>
								</div>
							</div>
							
							<p>
								<label for="noo_menu_use_search"><?php echo __('Search (on the right)',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('search')?>" id="noo_menu_use_search" class="form-control">
									<option <?php if(noo_menu_get_option('search','yes') == 'yes'): ?> selected="selected"<?php endif;?> value="yes"><?php echo __('Yes',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('search','yes') == 'no'): ?> selected="selected"<?php endif;?> value="no"><?php echo __('No',NOO_MENU) ?></option>
								</select>
							</p>
							<div id="noomenuSearch" style="clear: both;display:<?php if(noo_menu_get_option('search','yes') == 'yes'): ?>block<?php else: ?>none<?php endif?>">
								<p>
									<label for="noo_menu_use_search"><?php echo __('Show Search Form when',NOO_MENU) ?></label>
									<select name="<?php echo noo_menu_get_option_name('search_show')?>" id="noo_menu_use_search" class="form-control">
										<option <?php if(noo_menu_get_option('search_show','hover') == 'hover'): ?> selected="selected"<?php endif;?> value="hover"><?php echo __('Show when Hover',NOO_MENU) ?></option>
										<option <?php if(noo_menu_get_option('search_show','hover') == 'alway'): ?> selected="selected"<?php endif;?> value="alway"><?php echo __('Always Show',NOO_MENU) ?></option>
									</select>
								</p>
								<?php noo_menu_element_color('search_background_color','Background Color','#fff',1)?>
								<?php noo_menu_element_color('search_icon_color','Icon Color','#333')?>
							</div>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_animation_div" class="postbox <?php echo postbox_classes('noo_animation_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Animation',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<label for="animation"><?php echo __('Animation',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('animation')?>" id="animation" class="form-control">
									<option <?php if(noo_menu_get_option('animation') == ''): ?> selected="selected"<?php endif;?> value=""><?php echo __('None',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('animation') == 'fading'): ?> selected="selected"<?php endif;?>value="fading"><?php echo __('Fading',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('animation') == 'slide'): ?> selected="selected"<?php endif;?>value="slide"><?php echo __('Slide',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('animation') == 'zoom'): ?> selected="selected"<?php endif;?>value="zoom"><?php echo __('Zoom',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('animation') == 'elastic'): ?> selected="selected"<?php endif;?>value="elastic"><?php echo __('Elastic',NOO_MENU) ?></option>
								</select>
							</p>
							<?php noo_menu_element_slider('animation_duration','Duration','0','500','400','ms') ?>
							<?php noo_menu_element_slider('animation_delay','Delay','0','1000','0','ms') ?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_mobile_action_div" class="postbox <?php echo postbox_classes('noo_mobile_action_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Mobile Action',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<label for="mobile_action"><?php echo __('Mobile Action',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('mobile_action')?>" id="mobile_action" class="form-control">
									<option <?php if(noo_menu_get_option('mobile_action','2') == '1'): ?> selected="selected"<?php endif;?> value="1"><?php echo __('Collapse with Toggle Button',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('mobile_action','2') == '2'): ?> selected="selected"<?php endif;?> value="2"><?php echo __('Collapse no Toggle Button',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('mobile_action','2') == '3'): ?> selected="selected"<?php endif;?> value="3"><?php echo __('Do nothing',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('mobile_action','2') == '4'): ?> selected="selected"<?php endif;?> value="4"><?php echo __('Select',NOO_MENU) ?></option>
								</select>
							</p>
						</div>
					</div>

					<div id="noo_sticky_setting_div" class="postbox <?php echo postbox_classes('noo_sticky_setting_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sticky Settings',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<input id="use_sticky_menu" type="checkbox"  name="<?php echo noo_menu_get_option_name('use_sticky_menu') ?>" value="yes" <?php if(noo_menu_get_option('use_sticky_menu') == 'yes'):?> checked="checked"<?php endif;?>>
								<label for="use_sticky_menu"><?php echo __('Use Sticky Menu'); ?></label>
							</p>
							<p>
								<label for="sticky_wrap"><?php echo __('Sticky Wrap',NOO_MENU)?></label>
								<input id="sticky_wrap" class="form-control" type="text" name="<?php echo noo_menu_get_option_name('sticky_wrap') ?>" value="<?php echo noo_menu_get_option('sticky_wrap','90%') ?>" size="7">
							</p>
							<p>
								<input id="sticky_hide_scroll_up" type="checkbox"  name="<?php echo noo_menu_get_option_name('sticky_hide_scroll_up') ?>" value="yes" <?php if(noo_menu_get_option('sticky_hide_scroll_up') == 'yes'):?> checked="checked"<?php endif;?>>
								<label for="sticky_hide_scroll_up"><?php echo __('Only Show Sticky on Scroll-Up'); ?></label>
							</p>
							<p>
								<label for="sticky_margin_top"><?php echo __('Sticky Margin Top (px)',NOO_MENU)?></label>
								<input id="sticky_margin_top" class="form-control" type="text" name="<?php echo noo_menu_get_option_name('sticky_margin_top') ?>" value="<?php echo noo_menu_get_option('sticky_margin_top',0) ?>" size="7">
							</p>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tab1">
		<br>
			<div class="noo-row">
				<div class="noo-span4 meta-box-sortables">
					<?php if(noo_menu_get_option('orientation','horizontal')=='horizontal'):?>
					<div id="noo_menu_bar_height_div" class="postbox <?php echo postbox_classes('noo_menu_bar_height_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Menu Bar Height',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('menu_bar_height','Height','30','500','40')?>
						</div>
					</div>
					<?php endif;?>
					<div id="noo_menu_bar_border_div" class="postbox <?php echo postbox_classes('noo_menu_bar_border_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Border',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('menu_bar_border_width','Border Width',0,20)?>
							<p>
								<label for="border-style"><?php echo __('Border Style',NOO_MENU) ?> </label>
								<?php noo_menu_border_style('menu_bar')?>
							</p>
							
							<?php noo_menu_element_color('menu_bar_boder_color','Border Color')?>
							<?php noo_menu_border_position('menu_bar')?>
						</div>
					</div>
					<div id="noo_menu_bar_border_radius_div" class="postbox <?php echo postbox_classes('noo_menu_bar_border_radius_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Radius Corners',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_meu_border_radius('menu_bar',false)?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_menu_bar_background_div" class="postbox <?php echo postbox_classes('noo_menu_bar_background_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Background',NOO_MENU)?></span></h3>
						<div class="inside">
						<?php noo_menu_background('menu_bar',true,true,false)?>
						</div>
					</div>
					<div id="noo_menu_bar_shadow_div" class="postbox <?php echo postbox_classes('noo_menu_bar_shadow_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Menu Bar Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('menu_bar')?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					
					<div id="noo_menu_bar_alignment_div" class="postbox <?php echo postbox_classes('noo_menu_bar_alignment_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Alignment',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<label for="top_menu_icon_position"><?php echo __('Alignment',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('menu_bar_alignment')?>" id="menu_bar_alignment" class="form-control">
									<option <?php if(noo_menu_get_option('menu_bar_alignment','right') == 'left'): ?> selected="selected"<?php endif;?> value="left"><?php echo __('Left',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('menu_bar_alignment','right') == 'right'): ?> selected="selected"<?php endif;?> value="right"><?php echo __('Right',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('menu_bar_alignment','right') == 'center'): ?> selected="selected"<?php endif;?> value="center"><?php echo __('Center',NOO_MENU) ?></option>
								</select>
							</p>
						</div>
					</div>
					
					<div id="noo_menu_bar_padding_div" class="postbox <?php echo postbox_classes('noo_menu_bar_padding_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
							<h3 class="hndle"><span><?php echo __('Padding',NOO_MENU)?></span></h3>
							<div class="inside">
							<?php noo_menu_paddings('menu_bar',false,true,false,true,0,0,0,0,'')?>
							</div>
					</div>
					<div id="noo_menu_bar_margin_div" class="postbox <?php echo postbox_classes('noo_menu_bar_margin_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Margin',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_margins('menu_bar',false,true,false,true,0,0,0,0,'')?>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="tab-pane fade" id="tab2">
		<br>
			<div class="noo-row">
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_top_menu_height_div" class="postbox <?php echo postbox_classes('noo_top_menu_height_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Top Menu Height',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('top_menu_height','Height','30','','30')?>
							<?php if(noo_menu_get_option('orientation','horizontal')=='horizontal'):?>
							<?php noo_menu_element_slider('top_menu_vertical_position','Top Menu Vertical Position','','','0')?>
							<?php endif;?>
						</div>
					</div>
					<div id="noo_top_menu_border_div" class="postbox <?php echo postbox_classes('noo_top_menu_border_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Border',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('top_menu_border_width','Border Width',0,20)?>
							<p>
								<label for="border-style"><?php echo __('Border Style',NOO_MENU) ?> </label>
								<?php noo_menu_border_style('top_menu')?>
							</p>
							<?php noo_menu_element_color('top_menu_boder_color','Border Color')?>
							<?php noo_menu_border_position('top_menu')?>
						</div>
					</div>
					<div id="noo_top_menu_corner_radius_div" class="postbox <?php echo postbox_classes('noo_top_menu_corner_radius_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Corner Radius ',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_meu_border_radius('top_menu','')?>
						</div>
					</div>
					<div id="noo_top_menu_background_div" class="postbox <?php echo postbox_classes('noo_top_menu_background_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Background',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_background('top_menu',true,true,'')?>
						</div>
					</div>
					
					
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_top_menu_font_div" class="postbox <?php echo postbox_classes('noo_top_menu_font_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Font',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<?php noo_menu_google_font('top_menu');?>
								<?php /*
								<label for="top_menu_font"><?php echo __('Font',NOO_MENU) ?> </label>
									<select class="form-control" onchange="updatemenu()" id="top_menu_font" name="<?php echo noo_menu_get_option_name('top_menu_font')?>">
									  <option value="arial" <?php if(noo_menu_get_option('top_menu_font','arial') == 'arial'):?>selected="selected"<?php endif;?>><?php echo __('Arial',NOO_MENU) ?></option>
									  <option value="arial black" <?php if(noo_menu_get_option('top_menu_font','arial') == 'arial black'):?>selected="selected"<?php endif;?>><?php echo __('Arial Black',NOO_MENU) ?></option>
									  <option value="comic sans ms" <?php if(noo_menu_get_option('top_menu_font','arial') == 'comic sans ms'):?>selected="selected"<?php endif;?>><?php echo __('Comic Sans MS',NOO_MENU) ?></option>
									  <option value="courier new" <?php if(noo_menu_get_option('top_menu_font','arial') == 'courier new'):?>selected="selected"<?php endif;?>><?php echo __('Courier New',NOO_MENU) ?></option>
									  <option value="georgia" <?php if(noo_menu_get_option('top_menu_font','arial') == 'georgia'):?>selected="selected"<?php endif;?>><?php echo __('Georgia',NOO_MENU) ?></option>
									  <option value="helvetica" <?php if(noo_menu_get_option('top_menu_font','arial') == 'helvetica'):?>selected="selected"<?php endif;?>><?php echo __('Helvetica',NOO_MENU) ?></option>
									  <option value="impact" <?php if(noo_menu_get_option('top_menu_font','arial') == 'impact'):?>selected="selected"<?php endif;?>><?php echo __('Impact',NOO_MENU) ?></option>
									  <option value="times new roman" <?php if(noo_menu_get_option('top_menu_font','arial') == 'times new roman'):?>selected="selected"<?php endif;?>><?php echo __('Times New Roman',NOO_MENU) ?></option>
									  <option value="trebuchet ms" <?php if(noo_menu_get_option('top_menu_font','arial') == 'trebuchet ms'):?>selected="selected"<?php endif;?>><?php echo __('Trebuchet MS',NOO_MENU) ?></option>
									  <option value="verdana" <?php if(noo_menu_get_option('top_menu_font','arial') == 'verdana'):?>selected="selected"<?php endif;?>><?php echo __('Verdana',NOO_MENU) ?></option>
									</select>
								 */ ?>
							</p>
							<p>
								<label for="top_menu_font_style"><?php echo __('Font Style',NOO_MENU) ?> </label>
									<select class="form-control" onchange="updatemenu()" id="top_menu_font_style" name="<?php echo noo_menu_get_option_name('top_menu_font_style')?>">
									  <option value="normal" <?php if(noo_menu_get_option('top_menu_font_style','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
									  <option value="italic" <?php if(noo_menu_get_option('top_menu_font_style','normal') == 'italic'):?>selected="selected"<?php endif;?>><?php echo __('Italic',NOO_MENU) ?></option>
									  <option value="oblique" <?php if(noo_menu_get_option('top_menu_font_style','normal') == 'oblique'):?>selected="selected"<?php endif;?>><?php echo __('Oblique',NOO_MENU) ?></option>
									</select>
							</p>
							<p>
								<label for="top_menu_font_weight"><?php echo __('Font Weight',NOO_MENU) ?> </label>
									<select class="form-control" onchange="updatemenu()" id="top_menu_font_weight" name="<?php echo noo_menu_get_option_name('top_menu_font_weight')?>">
									  <option value="normal" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
									  <option value="bold" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'bold'):?>selected="selected"<?php endif;?>><?php echo __('Bold',NOO_MENU) ?></option>
									  <option value="bolder" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'bolder'):?>selected="selected"<?php endif;?>><?php echo __('Bolder',NOO_MENU) ?></option>
									  <option value="lighter" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'lighter'):?>selected="selected"<?php endif;?>><?php echo __('Lighter',NOO_MENU) ?></option>
									</select>
							</p>
							<?php noo_menu_element_slider('top_menu_font_size','Font Size','','','14')?>
							<?php noo_menu_element_color('top_menu_color','Font Color','#0088CC')?>
							<p>
								<input id="top_menu_font_uppercase" onchange="updatemenu()" type="checkbox" value="yes" <?php if(noo_menu_get_option('top_menu_font_uppercase') == 'yes'):?> checked="checked"<?php endif;?> name="<?php echo noo_menu_get_option_name('top_menu_font_uppercase')?>">
								<label for="top_menu_font_uppercase"><?php echo __('Use UPPERCASE',NOO_MENU)?></label>
							</p>
						</div>
					</div>
					<div id="noo_top_menu_font_shadow_div" class="postbox <?php echo postbox_classes('noo_top_menu_font_shadow_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Text Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('top_menu_font',false,false)?>
						</div>
					</div>
					
					<div id="noo_top_menu_icon_div" class="postbox <?php echo postbox_classes('noo_top_menu_icon_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Icon',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('top_menu_icon_size','Size','','','14')?>
							<?php noo_menu_element_color('top_menu_icon_color','Color','#0088CC')?>
							<p>
								<label for="top_menu_icon_position"><?php echo __('Position',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('top_menu_icon_position')?>" id="top_menu_icon_position" class="form-control">
									<option <?php if(noo_menu_get_option('top_menu_icon_position','left') == 'left'): ?> selected="selected"<?php endif;?> value="left"><?php echo __('Left',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('top_menu_icon_position','left') == 'right'): ?> selected="selected"<?php endif;?> value="right"><?php echo __('Right',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('top_menu_icon_position','left') == 'above'): ?> selected="selected"<?php endif;?> value="above"><?php echo __('Above',NOO_MENU) ?></option>
								</select>
							</p>
						</div>
					</div>
					
					
				</div>
				<div class="noo-span4 meta-box-sortables">
					
					<div id="noo_top_menu_padding_div" class="postbox <?php echo postbox_classes('noo_top_menu_padding_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Padding',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_paddings('top_menu',false,true,false,true,6,12,6,12,'')?>
						</div>
					</div>
					<div id="noo_top_menu_margin_div" class="postbox <?php echo postbox_classes('noo_top_menu_margin_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Margin',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_margins('top_menu',false,true,false,true,0,0,0,0,'')?>
						</div>
					</div>
					<div id="noo_top_menu_box_shadow_div" class="postbox <?php echo postbox_classes('noo_top_menu_box_shadow_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Box Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('top_menu')?>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="tab-pane fade" id="tab3">
		<br>
			<div class="noo-row">
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_top_menu_hover_text_color_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_text_color_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Text Color',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_color('top_menu_hover_color','Color')?>
						</div>
					</div>
					<div id="noo_top_menu_hover_text_shadow_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_text_shadow_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Text Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('top_menu_hover_font',false,false)?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_top_menu_hover_background_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_background_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Background',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_background('top_menu_hover',true,true,'')?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_top_menu_hover_border_color_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_border_color_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Border Color',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_color('top_menu_hover_boder_color','Color')?>
						</div>
					</div>
					
					<div id="noo_top_menu_hover_icon_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_icon_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Icon',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_color('top_menu_hover_icon_color','Color','#0088CC')?>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
		<div class="tab-pane fade" id="tab4">
		<br>
			<div class="noo-row">
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_sub_menu_box_border_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_border_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Box Border',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('sub_menu_border_width','Border Width',0,20)?>
							<p>
								<label><?php echo __('Border Style',NOO_MENU) ?> </label>
								<?php noo_menu_border_style('sub_menu')?>
							</p>
							<?php noo_menu_element_color('sub_menu_border_color','Border Color')?>
							<?php noo_menu_border_position('sub_menu')?>
						</div>
					</div>
					<div id="noo_sub_menu_border_div" class="postbox <?php echo postbox_classes('noo_sub_menu_border_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Item Border',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('sub_menu_item_border_width','Border Width',0,20)?>
							<p>
								<label><?php echo __('Border Style',NOO_MENU) ?> </label>
								<?php noo_menu_border_style('sub_menu_item')?>
							</p>
							<?php noo_menu_element_color('sub_menu_item_border_color','Border Color')?>
							<?php noo_menu_border_position('sub_menu_item')?>
						</div>
					</div>
					<div id="noo_sub_menu_box_border_radius_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_border_radius_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Box Corner Radius',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_meu_border_radius('sub_menu','')?>
						</div>
					</div>
					
					<div id="noo_sub_menu_icon_div" class="postbox <?php echo postbox_classes('noo_sub_menu_icon_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Icon',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_slider('sub_menu_icon_size','Size','','','14')?>
							<?php noo_menu_element_color('sub_menu_icon_color','Color','#0088CC')?>
							<p>
								<label for="sub_menu_icon_position"><?php echo __('Position',NOO_MENU) ?></label>
								<select name="<?php echo noo_menu_get_option_name('sub_menu_icon_position')?>" id="sub_menu_icon_position" class="form-control">
									<option <?php if(noo_menu_get_option('sub_menu_icon_position','left') == 'left'): ?> selected="selected"<?php endif;?> value="left"><?php echo __('Left',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('sub_menu_icon_position','left') == 'right'): ?> selected="selected"<?php endif;?> value="right"><?php echo __('Right',NOO_MENU) ?></option>
									<option <?php if(noo_menu_get_option('sub_menu_icon_position','left') == 'above'): ?> selected="selected"<?php endif;?> value="above"><?php echo __('Above',NOO_MENU) ?></option>
								</select>
							</p>
						</div>
					</div>
					
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_sub_menu_font_div" class="postbox <?php echo postbox_classes('noo_sub_menu_font_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Font',NOO_MENU)?></span></h3>
						<div class="inside">
							<p>
								<?php noo_menu_google_font('sub_menu');?>
								<?php /*
								<label for="sub_menu_font"><?php echo __('Font',NOO_MENU) ?> </label>
									<select class="form-control" onchange="updatemenu()" id="sub_menu_font" name="<?php echo noo_menu_get_option_name('sub_menu_font')?>">
									  <option value="arial" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'arial'):?>selected="selected"<?php endif;?>><?php echo __('Arial',NOO_MENU) ?></option>
									  <option value="arial black" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'arial black'):?>selected="selected"<?php endif;?>><?php echo __('Arial Black',NOO_MENU) ?></option>
									  <option value="comic sans ms" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'comic sans ms'):?>selected="selected"<?php endif;?>><?php echo __('Comic Sans MS',NOO_MENU) ?></option>
									  <option value="courier new" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'courier new'):?>selected="selected"<?php endif;?>><?php echo __('Courier New',NOO_MENU) ?></option>
									  <option value="georgia" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'georgia'):?>selected="selected"<?php endif;?>><?php echo __('Georgia',NOO_MENU) ?></option>
									  <option value="helvetica" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'helvetica'):?>selected="selected"<?php endif;?>><?php echo __('Helvetica',NOO_MENU) ?></option>
									  <option value="impact" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'impact'):?>selected="selected"<?php endif;?>><?php echo __('Impact',NOO_MENU) ?></option>
									  <option value="times new roman" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'times new roman'):?>selected="selected"<?php endif;?>><?php echo __('Times New Roman',NOO_MENU) ?></option>
									  <option value="trebuchet ms" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'trebuchet ms'):?>selected="selected"<?php endif;?>><?php echo __('Trebuchet MS',NOO_MENU) ?></option>
									  <option value="verdana" <?php if(noo_menu_get_option('sub_menu_font','arial') == 'verdana'):?>selected="selected"<?php endif;?>><?php echo __('Verdana',NOO_MENU) ?></option>
									</select>
									
									*/  ?>
							</p>
							<p>
								<label for="sub_menu_font_style"><?php echo __('Font Style',NOO_MENU) ?> </label>
									<select class="form-control" onchange="updatemenu()" id="sub_menu_font_style" name="<?php echo noo_menu_get_option_name('sub_menu_font_style')?>">
									  <option value="normal" <?php if(noo_menu_get_option('sub_menu_font_style','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
									  <option value="italic" <?php if(noo_menu_get_option('sub_menu_font_style','normal') == 'italic'):?>selected="selected"<?php endif;?>><?php echo __('Italic',NOO_MENU) ?></option>
									  <option value="oblique" <?php if(noo_menu_get_option('sub_menu_font_style','normal') == 'oblique'):?>selected="selected"<?php endif;?>><?php echo __('Oblique',NOO_MENU) ?></option>
									</select>
							</p>
							<p>
								<label for="sub_menu_font_weight"><?php echo __('Font Weight',NOO_MENU) ?> </label>
									<select class="form-control" onchange="updatemenu()" id="sub_menu_font_weight" name="<?php echo noo_menu_get_option_name('sub_menu_font_weight')?>">
									  <option value="normal" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
									  <option value="bold" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'bold'):?>selected="selected"<?php endif;?>><?php echo __('Bold',NOO_MENU) ?></option>
									  <option value="bolder" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'bolder'):?>selected="selected"<?php endif;?>><?php echo __('Bolder',NOO_MENU) ?></option>
									  <option value="lighter" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'lighter'):?>selected="selected"<?php endif;?>><?php echo __('Lighter',NOO_MENU) ?></option>
									</select>
							</p>
							<?php noo_menu_element_slider('sub_menu_font_size','Font Size','','','14')?>
							<?php noo_menu_element_color('sub_menu_color','Font Color','#333333')?>
							<p>
								<input id="sub_menu_font_uppercase" type="checkbox" onchange="updatemenu()" value="yes" <?php if(noo_menu_get_option('sub_menu_font_uppercase') == 'yes'):?> checked="checked"<?php endif;?> name="<?php echo noo_menu_get_option_name('sub_menu_font_uppercase')?>">
								<label for="sub_menu_font_uppercase"><?php echo __('Use UPPERCASE',NOO_MENU)?></label>
							</p>
						</div>
					</div>
					<div id="noo_sub_menu_text_shadown_div" class="postbox <?php echo postbox_classes('noo_sub_menu_text_shadown_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Text Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('sub_menu_font',false,false)?>
						</div>
					</div>
					<div id="noo_sub_menu_box_shadow_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_shadow_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Box Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('sub_menu_box')?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					
					<div id="noo_sub_menu_box_background_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_background_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Box Background',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_background('sub_menu',true,true,'')?>
						</div>
					</div>
					<div id="noo_sub_menu_padding_div" class="postbox <?php echo postbox_classes('noo_sub_menu_padding_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Item Padding',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_paddings('sub_menu_item',true,true,true,true,5,20,5,20,'')?>
						</div>
					</div>
					<div id="noo_sub_menu_box_padding_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_padding_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Sub Menu Box Padding',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_paddings('sub_menu_box',true,true,true,true,5,5,5,5,'')?>
						</div>
					</div>
					
					
					
				</div>
			</div>
		</div>
	
		<div class="tab-pane fade" id="tab5">
		<br>
			<div class="noo-row">
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_sub_menu_hover_text_color_div" class="postbox <?php echo postbox_classes('noo_sub_menu_hover_text_color_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Text Color',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php 
							noo_menu_element_color('sub_menu_hover_color','Color')
							?>
						</div>
					</div>
					<div id="noo_sub_menu_hover_text_shadow_div" class="postbox <?php echo postbox_classes('noo_sub_menu_hover_text_shadow_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Text Shadow',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_shadow('sub_menu_hover_font',false,false)?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_sub_menu_hover_background_div" class="postbox <?php echo postbox_classes('noo_menu_bar_border_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Background',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_background('sub_menu_hover',true,true,'')?>
						</div>
					</div>
				</div>
				<div class="noo-span4 meta-box-sortables">
					<div id="noo_sub_menu_hover_boder_color_div" class="postbox <?php echo postbox_classes('noo_sub_menu_hover_boder_color_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Hover Border Color',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_color('sub_menu_item_hover_boder_color','Color')?>
						</div>
					</div>
					
					<div id="noo_sub_menu_hover_icon_div" class="postbox <?php echo postbox_classes('noo_sub_menu_hover_icon_div', NOO_MENU) ?>">
						<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
						<h3 class="hndle"><span><?php echo __('Icon',NOO_MENU)?></span></h3>
						<div class="inside">
							<?php noo_menu_element_color('sub_menu_hover_icon_color','Color','#0088CC')?>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tab6">
			<br>
			<div class="noo-row">
				<div class="noo-span12">
					<textarea rows="" cols="" placeholder="<?php echo __('Add Custom style in here',NOO_MENU)?>" name="<?php echo noo_menu_get_option_name('custom_style')?>" style="width: 99%;height: 100px"><?php echo noo_menu_get_option('custom_style')?></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
</div>