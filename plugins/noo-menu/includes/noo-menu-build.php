<?php

class Noo_Menu_Build {
	
	protected $menu_items= null ;
	protected $children = array();
	protected $items = array();
	protected $output = '';
	protected $params = null;
	protected $settings = null;
	protected $top_level_caption =  false;
	protected $front_end = false;
	protected $args = array();
	
	protected $menu_id;
	
	
	public function __construct($menu_id=null){
		if(!empty($menu_id)){
			$this->menu_id = $menu_id;
			if(!$this->front_end){
				$menu_object = wp_get_nav_menu_object($menu_id);
				if ( $menu_object && ! is_wp_error($menu_object)){
					$this->menu_items = wp_get_nav_menu_items( $menu_object->term_id, array( 'update_post_term_cache' => false ) );
				}
			}
		}
	}
	
	protected function _render_menu_items($menu_items){
		
		if(empty($menu_items))
			return '';

		if(!$this->front_end){
			$unsorted_menu_items = array();
			if ( ! empty( $_POST['menu-item-db-id'] ) ) {
				$_keyArr = array_keys((array)$_POST['menu-item-db-id']);
				
	 			foreach( (array) $_POST['menu-item-db-id'] as $_key => $k ) { 				
// 	 				$menu_obj->menu_item_parent = strval( (int) $_POST['menu-item-parent-id'][$_key] );
	 				$menu_obj = get_post($_key);
	 				if ( ! empty( $menu_obj->ID ) ) {
	 					$menu_obj = wp_setup_nav_menu_item( $menu_obj );
	 					$menu_obj->menu_item_parent = strval( (int) $_POST['menu-item-parent-id'][$_key] );
	 					
	 					if(!in_array($menu_obj->menu_item_parent, $_keyArr))
							$menu_obj->menu_item_parent = 0;
	 						
	 					$unsorted_menu_items[$menu_obj->ID] = $menu_obj;
	 				}
	 				
	 			}
	 			
	 			$menu_items = $unsorted_menu_items;
	 		}
		}
		

		if($this->front_end)
			_wp_menu_item_classes_by_context($menu_items);

		$children = array();
		foreach ($menu_items as $item){
			
            $pt = $item->menu_item_parent;
            $list = (isset($children[$pt]) && !empty($children[$pt])) ? $children[$pt] : array();
            array_push($list, $item);
            $children[$pt] = $list;
		}
		
		$lists = noo_menu_treerecurse(0, array(), $children);
		
		
		foreach ($lists as $item){
			$item->title = htmlspecialchars($item->title);
			$item->level = $item->level + 1;
			$key = 'item-'.$item->ID;
			$setting = isset($this->settings[$key]) ? $this->settings[$key] : array();
			// decode html tag
			if (isset($setting['caption']) && $setting['caption']) $setting['caption'] = str_replace(array('[lt]','[gt]'), array('<','>'), $setting['caption']);
			if ($item->level == 1 && isset($setting['caption']) && $setting['caption']) $this->top_level_caption = true;
				
			
			
			$item->class = '';
			$item->mega = 0;
			$item->group = 0;
			$item->dropdown = 0;
				
			//$item->target = '';
			if (isset($setting['group'])) {
				$item->group = 1;
			} else {
				if ($this->front_end){
					if (($item->children && (!isset($setting['hidesub']))) || isset($setting['sub'])) {
						$item->dropdown = 1;
					}
				}else{
					if ($item->children || isset($setting['sub'])) {
						$item->dropdown = 1;
					}
				}
			}
		
				
			$item->mega = $item->group || $item->dropdown;
				
				
			if ($item->mega) {
				if (!isset($setting['sub'])) $setting['sub'] = array();
				if ($item->children && (!isset($setting['sub']['rows']) || !count($setting['sub']['rows']))) {
					$c = $item->frist_children->ID;
					$setting['sub'] = array('rows'=>array(array(array('width'=>12, 'item'=>$c))));
				}
			}
				
			$item->setting = $setting;
		
			$item->url  = esc_url($item->url);
				
			$parent = isset($this->children[$item->menu_item_parent]) ? $this->children[$item->menu_item_parent] : array();
			$parent[] = $item;
			$this->children[$item->menu_item_parent] = $parent;
			$this->items[$item->ID] = $item;
		}
	}
	
	protected function _render_nav($pitem, $start = 0, $end = 0) {
	
		if ($start > 0) {
			if (!isset($this->items[$start]))
				return;
			$pid     = $this->items[$start]->menu_item_parent;
			$items   = array();
			$started = false;
			foreach ($this->children[$pid] as $item) {
				if ($started) {
					if ($item->ID == $end)
						break;
					$items[] = $item;
				} else {
					if ($item->ID == $start) {
						$started = true;
						$items[] = $item;
					}
				}
			}
			if (!count($items))
				return;
		} else if ($start === 0) {
			$pid = $pitem->ID;
			if (!isset($this->children[$pid]))
				return;
			$items = $this->children[$pid];
		} else {
			//empty menu
			return;
		}
	
		$beginnav = $this->__('beginnav', array(
				'item' => $pitem
		));
		$itemHtml = '';
		foreach ($items as $item) {
			$itemHtml .= $this->_render_item($item);
		}
	
		$endnav = $this->__('endnav', array(
				'item' => $pitem
		));
		if ($itemHtml == ''){
			return '';
		}
		return $beginnav.$itemHtml.$endnav;
	}
	
	protected function _render_item ($item) {
		// item content
		$setting = $item->setting;
	
	
		$megaHtml = '';
		if ($item->mega) {
			$megaHtml .= $this->_render_mega($item);
		}
		if ($megaHtml == ''){
			$item->group = 0;
			$item->dropdown = 0;
			$item->mega = 0;
		}
	
		$html = $this->__('beginitem', array ('item'=>$item, 'setting'=>$setting));
	
		$itemHtml = $this->__('item', array ('item'=>$item, 'setting'=>$setting));
		$html .= $itemHtml;
		if ($megaHtml != ''){
			$html .= $megaHtml;
		}
		$html .= $this->__('enditem', array ('item'=>$item));
		return $html;
	}
	
	protected  function _render_mega ($item) {

		
		$key       = 'item-' . $item->ID;
		$setting   = $item->setting;
		$sub       = $setting['sub'];
		$items     = isset($this->children[$item->ID]) ? $this->children[$item->ID] : array();
		$firstitem = count($items) ? $items[0]->ID : 0;
	
	
		$endItems = array();
		$k1       = $k2 = 0;
		foreach ($sub['rows'] as $row) {
			foreach ($row as $col) {
				if (!isset($col['widget'])) {
					if ($k1) {
						$k2 = $col['item'];
						if (!isset($this->items[$k2]) || $this->items[$k2]->menu_item_parent != $item->ID)
							break;
						$endItems[$k1] = $k2;
					}
					$k1 = $col['item'];
				}
			}
		}
	
		$html = '';
		$endItems[$k1] = 0;
		$beginmega = $this->__('beginmega', array(
				'item' => $item
		));
		$firstitemscol = true;
		$rowHtml = '';
		foreach ($sub['rows'] as $row) {
			$beginrow = $this->__('beginrow');
			$colHtml = '';
			foreach ($row as $col) {
				if (isset($col['widget'])) {
					$beginwidget = $this->__('begincol', array('setting' => $col));
					$endwidget = $this->__('endcol');
					if ($widget = $this->_render_widget($col['widget'])){
						$colHtml .= $beginwidget.$widget.$endwidget;
					}
						
				} else {
					if (!isset($endItems[$col['item']])){
						continue;
					}
						
					$begincol = $this->__('begincol', array('setting' => $col));
					$toitem    = $endItems[$col['item']];
					$startitem = $firstitemscol ? $firstitem : $col['item'];
					$subNav = $this->_render_nav($item, $startitem, $toitem);
					$firstitemscol = false;
					$endcol = $this->__('endcol');
					if ($subNav != ''){
						$colHtml .= $begincol.$subNav.$endcol;
					}
				}
	
			}
			$endrow = $this->__('endrow');
			if ($colHtml != ''){
				$rowHtml .= $beginrow.$colHtml.$endrow;
			}
		}
		$endmega =$this->__('endmega');
		if ($rowHtml == ''){
			return '';
		}
		return $beginmega.$rowHtml.$endmega;
	}
	
	protected function _render_widget($id){
		if($widget = noo_menu_get_widget($id)){
			if($this->front_end){
				ob_start();
				the_widget($widget->class,$widget->params);
				return ob_get_clean();
			}
			$title = $widget->name;
			if(property_exists($widget,'params')){
				if(isset($widget->params['title'])){
					$title .= ': '.$widget->params['title'].'';
				}
			}
			return '<div class="widget-title-template"><i class="icon-code"></i>'.$title.'</div>';
		}
		return '';
	}
	
	protected function __($tmpl, $args = array()) {
		$args ['menu'] = $this;
		$func = '_'.$tmpl;
		if (method_exists($this, $func)) {
			return $this->$func($args)."\n";
		} else {
			return "$tmpl\n";
		}
	}
	
	protected function _beginmenu ($args) {
		$menu = $args['menu'];
		$animation = '';
		if (trim($menu->_get_param('trigger','hover')) == 'hover'){
			$animation = $menu->_get_param('animation','');
		}
	
		$data = ' data-trigger="'.$menu->_get_param('trigger','hover').'"';
		$animation_duration = $menu->_get_param ('duration',200);
		$orientation = $menu->_get_param('orientation','horizontal');
		
		
		$cls = ' class="'.($this->front_end ? $this->args->container_class : '').' noo-megamenu '.$orientation.' '.($menu->_get_param('mobile_action',2) != '3' ? 'noocollapse' : '').''.($animation ? ' animate '.$animation : '').'"';
		if ($menu->_get_param('sticky',0)){
			$data .= ' data-sticky="1"';
		}
		if ($menu->_get_param('sliding',0)){
			$data .= ' data-sliding="1"';
		}
		$div = '';
		$data .= $animation && $animation_duration ? ' data-duration="'.$animation_duration.'"' : '';
		
		if($this->front_end && $menu->_get_param('logo','yes') == 'yes'){
			
			$logo = NOO_MENU_URL.'assets/images/noo-logo.png';
			if($menu->_get_param('menu_logo',0) > 0){
				$logo = wp_get_attachment_url($menu->_get_param('menu_logo',0));
			}
			$div .='<span class="noo-menu-logo"><a href="'.home_url().'" title="'.get_bloginfo('title').'"><img src="'.$logo.'"/></a></span>';
		}
		$div .= "<div$cls$data>";
		return $div;
		}

		protected function _endmenu ($args) {
			$div ='';
			$div .= '</div>';
			return $div;
		}

		protected function _beginnav ($args) {
			$item = $args['item'];
			$menu = $args['menu'];
			$cls = '';
			
			if (!$item) {
				$cls = 'clearfix noo-nav level0';
			} else {
				$cls .= ' mega-nav';
				$cls .= ' level'.$item->level;
			}
			 
			if ($cls) $cls = 'class="'.trim($cls).'"';

			return '<ul '.$cls.'>';
		}

		protected function _endnav ($args) {
			$item = $args['item'];
			$menu = $args['menu'];
			$ul = '';
			if (!$item && $this->front_end && $menu->_get_param('search','yes') == 'yes') {
				$ul .='<li class="noo-menu-search-box">';
				$ul .= noo_menu_get_search_form();
				$ul .='</li>';
			}
			return $ul.'</ul>';
		}

		protected function _beginmega ($args) {
			$item = $args['item'];
			$setting = $item->setting;
			$sub = $setting['sub'];
			$cls = 'noo-nav-child '.($item->dropdown ? 'dropdown-menu mega-dropdown-menu' : 'mega-group-ct');
			$style = '';
			$data = '';
			//if (isset($setting['class'])) $data .= " data-class=\"{$setting['class']}\"";
			if (isset($sub['class'])) {
				$data .= " data-class=\"{$sub['class']}\"";
				$cls  .= " {$sub['class']}";
			}
			
			if (isset($setting['alignsub']) && $setting['alignsub'] == 'justify') {
				$cls .= " noo-span12";
			} else {
				if (isset($sub['width'])) {
					if ($item->dropdown) $style = " style=\"width:{$sub['width']}\"";
					$data .= " data-width=\"{$sub['width']}\"";
				}
			}

			if ($cls) $cls = 'class="'.trim($cls).'"';

			return "<div $cls $style $data><div class=\"mega-dropdown-inner\">";
	}
	
	protected function _endmega ($args) {
		return '</div></div>';
	}
	
	protected function _beginrow ($args) {
		return '<div class="noo-row">';
	}
	
	protected function _endrow ($args) {
		return '</div>';
	}
	
	protected  function _begincol ($args) {
		$setting = isset($args['setting']) ? $args['setting'] : array();
		$width = isset($setting['width']) ? $setting['width'] : '12';
		$data = '';
		if (!$this->front_end)
			$data = "data-width=\"$width\"";
		
		$cls = "noo-col noo-span$width";
		
		if (isset($setting['widget'])) {
			$cls .= " noo-col-widget";
			if (!$this->front_end)
				$data .= " data-widget=\"{$setting['widget']}\"";
		} else {
			$cls .= " noo-col-nav";
		}
		if (isset($setting['class'])) {
			$cls .= " {$setting['class']}";
			if (!$this->front_end)
				$data .= " data-class=\"{$setting['class']}\"";
		}
		if (isset($setting['hidewcol'])) {
			if (!$this->front_end)
				$data .= " data-hidewcol=\"1\"";
			$cls .= " hidden-collapse";
		}
		return "<div class=\"$cls\" $data><div class=\"mega-inner\">";
	}
	
	protected function _endcol ($args) {
	return '</div></div>';
	}
	
	protected function _beginitem ($args) {
		$item = $args['item'];
		$menu = $args['menu'];
		$setting = $item->setting;
		$cls = 'noo-nav-item '.$item->class;
	
		if ($item->dropdown) {
			$cls .= $item->level == 1 ? 'dropdown' : 'dropdown-submenu';
		}
						
		if ($item->mega) $cls .= ' mega';
		if ($item->group) $cls .= ' mega-group';
	
		$id = $item->ID;
		
		$data = "data-id=\"{$id}\" data-level=\"{$item->level}\"";
		
		if ($item->group && !$this->front_end)
			$data .= " data-group=\"1\"";
			
		if (isset($setting['class'])) {
			if (!$this->front_end)
				$data .= " data-class=\"{$setting['class']}\"";
			$cls .= " {$setting['class']}";
		}
		if (isset($setting['alignsub'])) {
			if (!$this->front_end)
				$data .= " data-alignsub=\"{$setting['alignsub']}\"";
			$cls .= " mega-align-{$setting['alignsub']}";
		}
		if (isset($setting['hidesub']) && !$this->front_end)
			$data .= " data-hidesub=\"1\"";
		
		if (isset($setting['xicon']) && !$this->front_end)
			$data .= " data-xicon=\"{$setting['xicon']}\"";
	
		if (isset($setting['caption']) && !$this->front_end)
			$data .= " data-caption=\"".htmlspecialchars($setting['caption'])."\"";
		if (isset($setting['hidewcol'])) {
			if (!$this->front_end)
				$data .= " data-hidewcol=\"1\"";
			$cls .= " sub-hidden-collapse";
		}
	
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, array() ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . ' '.$cls.'"' : ' class="'.$cls.'"';
	
		return "<li $class_names $data>";
	}
	protected  function _enditem ($args) {
		return '</li>';
	}
	protected function _item ($args) {
		$item = $args['item'];
		$menu = $args['menu'];
		$setting = $item->setting;
	
		$args['title'] = $item->attr_title ? 'title="'.$item->attr_title.'" ' : '';
		$args['dropdown'] = '';
		$args['caret'] = '';
		$args['icon'] = '';
		$args['caption'] = '';
		$args['class'] = '';
	
		if($item->dropdown && $item->level < 2){
			$args['class'] .= ' dropdown-toggle';
			$args['dropdown'] = ' ';
			if($this->front_end){
				if($menu->_get_param('orientation','') == 'horizontal')
					$args['caret'] = '<b class="caret"></b>';
			}else{
				//if($menu->_get_param('orientation','') == 'horizontal')
					$args['caret'] = '<b class="caret"></b>';
			}
		}
	
		if ($item->group) $args['class'] .= ' mega-group-title';
	
	
		$args['label'] = '<span>'.$item->title.'</span>';
		if (isset($setting['xicon']) && $setting['xicon']) {
			$args['icon'] = '<i class="noo-icon '.$setting['xicon'].'"></i>';
				
		}
		if (isset($setting['caption']) && $setting['caption']) {
			$args['caption'] = '<span class="mega-caption">'.$setting['caption'].'</span>';
		} else if ($item->level==1 && $args['menu']->get('top_level_caption')) {
			$args['caption'] = '<span class="mega-caption mega-caption-empty">&nbsp;</span>';
		}
	
		$html = $this->_item_url ($args);
	
		return $html;
	}
	
	protected  function _item_url ($args) {
		$item = $args['item'];
		$menu = $args['menu'];
		$class = $args['class'];
	
		$title = $args['title'];
		$caret = $args['caret'];
		$label = $args['label'];
		$icon = $args['icon'];
		$caption = $args['caption'];
		$dropdown = $args['dropdown'];
	
		//$target = ! empty( $item->target ) ? 'target="_blank"': '';
		$rel    = ! empty( $item->xfn ) ? 'rel="'.$item->xfn.'"': '';
	
		$url = $item->url;
	
		$url = htmlspecialchars($url);
		
		$icon_right = '';
		if($item->level < 2){
			$top_menu_icon_position = $menu->_get_param('top_menu_icon_position','left');
			if($top_menu_icon_position == 'right'){
				$icon_right = $icon;
				$icon= '';
			}
			
			$class .= ' noo-menu-icon-'.$top_menu_icon_position;
		}else{
			$sub_menu_icon_position = $menu->_get_param('sub_menu_icon_position','left');
			if($sub_menu_icon_position == 'right'){
				$icon_right = $icon;
				$icon= '';
			}
				
			$class .= ' noo-menu-icon-'.$sub_menu_icon_position;
		}
		
		$link = "";
		switch ($item->target) :
			default:
				$link = "<a $rel class=\"$class\" href=\"$url\"  $title$dropdown>$icon$label$icon_right$caret$caption</a>";
			break;
			case '_blank':
				$link = "<a target=\"_blank\" $rel class=\"$class\" href=\"$url\" $title$dropdown>$icon$label$icon_right$caret$caption</a>";
				break;
		endswitch;
	
		
		return $link;
	}
	
	protected  function _get_param($key,$default=null){
		if (isset($this->params[$key]))
			return $this->params[$key];
		return $default;
	}
	
	protected function get($key){
		if ($this->$key)
			return $this->$key;
		return null;
	}
	
	public function output($options=array(),$is_front_end = false,$args = array()){

		$this->settings = $options['settings'];
		$this->params = $options['params'];
		$this->front_end = $is_front_end;
		$this->args = $args;
		
		$this->_render_menu_items($this->menu_items);
		
		$this->output .= $this->__('beginmenu');
		$keys = array_keys($this->items);
	
		if(count($keys)){
			$this->output .= $this->_render_nav(null, $keys[0]);
		}
		$this->output .= $this->__('endmenu');
	
	
		return $this->output;
	}
	
	
}