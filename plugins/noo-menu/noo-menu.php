<?php
/*
Plugin Name: Noo Menu
Plugin URI: http://codecanyon.net/item/noo-menu-wordpress-mega-menu-plugin/7873697
Description: NOO Menu is developed by <a href="http://codecanyon.net/user/NooTheme" target="_blank">NooTheme team</a> with non-tech-savvy users in our mind. The idea is to create a WP Mega Menu Plugin with the most intuitive backend interface ever and powerful enough to build highly customised menus. Support resources consist of <a href="http://blog.nootheme.com/kb-noo-menu-documentation" target="_blank">Documentation</a>, <a href="https://www.youtube.com/playlist?list=PL4ug4LqTBiRuwH69QbA3Ek5EUwrnzp8we" target="_blank">Video Tutorial</a> and <a href="http://blog.nootheme.com/kb-submit-ticket/" target="_blank">Ticket Support</a>.
Author: NooTheme
Version: 1.1.4
Author URI: http://codecanyon.net/user/NooTheme
*/

if (!defined('NOO_MENU')) {
	define('NOO_MENU','noo-menu');
}
if (!defined('NOO_MENU_VERSION')) {
	define('NOO_MENU_VERSION', '1.1.4');
}
if(!defined('NOO_MENU_URL')){
	define('NOO_MENU_URL', plugin_dir_url(__FILE__));
}
if(!defined('NOO_MENU_DIR')){
	define('NOO_MENU_DIR', dirname(__FILE__).DIRECTORY_SEPARATOR);
}

global $noo_menu_id,$noo_menu_style_id;
$noo_menu_id = $noo_menu_style_id = 0;

if(!class_exists('Noo_Menu',false)){
	if(!is_admin()){
		//require_once NOO_MENU_DIR.'noo-menu-demo-control.php';
	}
	require_once NOO_MENU_DIR.'includes/noo-menu-functions.php';
	require_once NOO_MENU_DIR.'includes/noo-menu-build.php';
	require_once NOO_MENU_DIR.'includes/noo-menu-select-walker.php';
	class Noo_Menu{
		protected static $_instance = null;
		
		public function __construct(){
			
			register_activation_hook( __FILE__, array( $this, 'install' ) );
			
			add_action('init',array(&$this,'init'));
			add_action('init',array(&$this, 'widget_init'),1);
			
			if(is_admin()){
				require_once NOO_MENU_DIR.'includes/noo-menu-admin.php';
				new Noo_Menu_Admin();
			}else{
				add_action('wp_print_scripts', array($this,'js_frontend'));
				add_action('wp_print_styles',array(&$this,'enqueue_styles'));
				//Filters
				add_filter('wp_nav_menu', array($this,'nav_menu'),10,10);	
			}
			add_shortcode('noomenu', array($this,'noomenu_shortcode'));
		}
		/**
		 * 
		 * @return Noo_Menu
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;

		}
		
		public function install(){
			$saved_style = get_option('noo_menu_style');
			if(empty($saved_style)){
				$preset = @file_get_contents(NOO_MENU_DIR.'assets/preset/preset.txt');
				add_option('noo_menu_style',maybe_unserialize(trim($preset)));
			}
		}
		public function widget_init(){
			require_once NOO_MENU_DIR.'includes/noo-menu-widget.php';
			
			register_widget('Noo_Menu_Widget');
		}
		public function init(){
			load_plugin_textdomain( NOO_MENU, false, basename(NOO_MENU_DIR) . '/languages' );
			
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			
			wp_register_style('noo-font-awesome', NOO_MENU_URL.'assets/fonts/awesome/css/font-awesome' . $suffix . '.css',array(),'4.0.3');
			wp_register_style('noo-menu',NOO_MENU_URL.'assets/css/style' . $suffix . '.css',array(),NOO_MENU_VERSION);
			wp_register_style('noo-menu-default',NOO_MENU_URL.'assets/css/default' . $suffix . '.css',array(),NOO_MENU_VERSION);
			wp_register_style('noo-menu-responsive',NOO_MENU_URL.'assets/css/responsive' . $suffix . '.css',array(),NOO_MENU_VERSION);
			wp_register_style('noo-menu-no-thing',NOO_MENU_URL.'assets/css/no-thing' . $suffix . '.css',array(),NOO_MENU_VERSION);
		}
		
		public function js_frontend(){
			wp_register_script('noo-menu-waypoints',NOO_MENU_URL.'assets/js/waypoints.min.js',array('jquery'),NOO_MENU_VERSION);
			wp_register_script('noo-menu-waypoints-sticky',NOO_MENU_URL.'assets/js/waypoints-sticky-custom.min.js',array('jquery'),NOO_MENU_VERSION);
			wp_register_script('noo-menu',NOO_MENU_URL.'assets/js/script.js',array('jquery','noo-menu-waypoints','noo-menu-waypoints-sticky'),NOO_MENU_VERSION);
			wp_enqueue_script('noo-menu');
			
		}
		
		public function enqueue_styles(){
			wp_enqueue_style('noo-font-awesome');
			wp_enqueue_style('noo-menu');
			$nav_menus = wp_get_nav_menus( array('orderby' => 'name') );
			if(!$nav_menus || is_wp_error($nav_menus)){
				return '';
			}
			$generator_css = '';
			$mobile_action = 2;
			$sticky_css = '';
			if(!empty($nav_menus)){
				foreach ($nav_menus as $nav_menu){
					$location = $nav_menu->term_id;
					$options = get_option('noo_menu'.$location);
					
					$top_menu_font = isset($options['top_menu_font_url']) ? $options['top_menu_font_url'] : '';
					if(!empty($top_menu_font) && $top_menu_font !='inherit')
						wp_enqueue_style('noo-top-menu-font'.$location,'//fonts.googleapis.com/css?'.$top_menu_font);

					$sub_menu_font = isset($options['sub_menu_font_url']) ? $options['sub_menu_font_url'] : '';
					if(!empty($sub_menu_font) && $sub_menu_font != 'inherit')
						wp_enqueue_style('noo-sub_menu-font'.$location,'//fonts.googleapis.com/css?'.$sub_menu_font);
					
					
					if(isset($options['use_sticky_menu']) && $options['use_sticky_menu'] == 'yes'){
						$sticky_css .= '.noo_menu_'.$location.'.noosticky .noo-megamenu{top:'.(isset($options['sticky_margin_top']) ? (int) $options['sticky_margin_top'] : (int) $options['sticky_margin_top']).'px;}';
					}

					$menu_style = isset($options['menu_style']) ? $options['menu_style']."\n" : '';
					$custom_style = isset($options['menu_style']) ? $options['custom_style']."\n" : '';
					$generator_css .= $menu_style.$custom_style;
					$mobile_action = isset($options['mobile_action']) ? $options['mobile_action'] : 2;
				}
			}
			if(defined('NOO_MENU_DEMO')){
				$generator_css = '1';
			}

			wp_add_inline_style('noo-menu-default',$sticky_css);
			wp_add_inline_style('noo-menu-default',$generator_css);
			wp_enqueue_style('noo-menu-default');
			if($mobile_action){
				wp_enqueue_style('noo-menu-responsive');
			}else{
				wp_enqueue_style('noo-menu-no-thing');
			}
			
		}
		
		public function nav_menu($nav_menu, $args){
	
			global $noo_menu_id;
			
			$menu = 0;
			// Get the nav menu based on the requested menu
			$menu = wp_get_nav_menu_object( $args->menu );
			
			// Get the nav menu based on the theme_location
			if ( ! $menu && $args->theme_location && ( $locations = get_nav_menu_locations() ) && isset( $locations[ $args->theme_location ] ) )
				$menu = wp_get_nav_menu_object( $locations[ $args->theme_location ] );
			
			

			if ( !$menu || is_wp_error($menu)){
				return $nav_menu;
			}
			
			$noo_menu_id = $menu->term_id;
			
			if(noo_menu_get_option('enable') < 0 ){
				return $nav_menu;
			}
			$output ='';
			if(noo_menu_get_option('mobile_action',2) == '4'){
				// If the menu exists, get its items.
				//if ( $menu && ! is_wp_error($menu) && !isset($menu_items) )
				$menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false ) );
					
				
				
				
				$items = '';
				$sorted_menu_items = array();
				// Set up the $menu_item variables
				_wp_menu_item_classes_by_context( $menu_items );
				
				foreach ( (array) $menu_items as $menu_item ) {
					$sorted_menu_items[ $menu_item->menu_order ] = $menu_item;
				}
				unset( $menu_items, $menu_item );
				
				$args->walker =  new Noo_Select_Walker_Nav_Menu();
				
				$items .= walk_nav_menu_tree( $sorted_menu_items,$args->depth,$args);
				unset($sorted_menu_items);
				$output .= sprintf('<select class="%1$s">%2$s</select>','noo-menu-select', $items );
				unset($items);
			}
			$output .= $this->toHtml($menu->term_id,$args);
			
			return $output;
		}
		

		protected function _is_rtl(){
			if ( function_exists( 'is_rtl' ) && is_rtl() )
				return true;
			return false;
		}
		
		public function toHtml($menu,$args){
			
			$rtl = $this->_is_rtl() ? ' noo-rtl ' : '';
			
			$menu_build = new Noo_Menu_Build($menu);
			$dropdown = noo_menu_get_option('horizontal_dropdown','down');
			if(noo_menu_get_option('orientation','horizontal') == 'vertical'){
				$dropdown = noo_menu_get_option('vertical_dropdown','down');
			}
			$alignment = 'noo-menu-align-'.noo_menu_get_option('menu_bar_alignment','right');
			$menu_build_options = array('settings'=>json_decode(noo_menu_get_option('menu_config'),true),'params'=>get_option('noo_menu'.$menu));
			$menuStyle = noo_menu_get_option('orientation','horizontal');
			$sticky = (noo_menu_get_option('use_sticky_menu',0) === 'yes') ? ' data-sticky="1"' : ' data-sticky="0"';
			$sticky_hide_up = (noo_menu_get_option('sticky_hide_scroll_up',0) === 'yes') ? ' data-hide-sticky="1"' : ' data-hide-sticky="0"';
			$output = "<div ".$sticky_hide_up." data-offset=\"".(int)noo_menu_get_option('sticky_margin_top',0)."\" ".$sticky." id=\"noo_menu_".$menu."\" class=\"".$alignment.' '.$rtl." noonav noo_menu_".$menu." noo-nav-mb".noo_menu_get_option('mobile_action',2)." ".$menuStyle."-".$dropdown."\">\n";
			
			if(noo_menu_get_option('mobile_action',2) == '1'){
				$output .= "<button class=\"button-bar noo-menu-collapse\" type=\"button\">\n";
				$output .= "<span class=\"icon-bar\"></span>\n";
				$output .= "<span class=\"icon-bar\"></span>\n";
				$output .= "<span class=\"icon-bar\"></span>\n";
				$output .= "</button>\n";
				$output .= "<a class=\"navbar-brand\" href=\"javascript:void(0)\">".noo_menu_get_option('reponsive_heading','Menu')."</a>\n";
			}
			$output .='<div>';
			$output .= $menu_build->output($menu_build_options,true,$args);
			$output .='</div>';
			$output .= "</div>\n";
			
			return $output;
		}
		
		public function noomenu_shortcode($atts, $content = null){
			extract( shortcode_atts( array(
				'menu'    => '',
			), $atts ) );
			
			if(empty($menu)){
				return '';
			}
			
			$menu_object = wp_get_nav_menu_object($menu );
			if ( !$menu_object || is_wp_error($menu_object)){
				return '';
			}

			global $noo_menu_id;
			$noo_menu_id = $menu_object->term_id;
			if(noo_menu_get_option('mobile_action',2) == '4'){
				$args = array( 'menu' => '', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'menu', 'menu_id' => '',
						'echo' => false, 'fallback_cb' => 'wp_page_menu', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth' => 0, 'walker' => '', 'theme_location' => '' );
				
				
	 			//return $this->toHtml($menu_object->term_id,$args);
				
				// If the menu exists, get its items.
				//if ( $menu && ! is_wp_error($menu) && !isset($menu_items) )
				$menu_items = wp_get_nav_menu_items( $menu_object->term_id, array( 'update_post_term_cache' => false ) );
				
				$output ='';
					
				$items = '';
				$sorted_menu_items = array();
				// Set up the $menu_item variables
				_wp_menu_item_classes_by_context( $menu_items );
					
				foreach ( (array) $menu_items as $menu_item ) {
					$sorted_menu_items[ $menu_item->menu_order ] = $menu_item;
				}
				unset( $menu_items, $menu_item );
				
				$args = (object) $args;
				$args->walker =  new Noo_Select_Walker_Nav_Menu();
					
				$items .= walk_nav_menu_tree( $sorted_menu_items,$args->depth,$args);
				unset($sorted_menu_items);
				$output .= sprintf('<select class="%1$s">%2$s</select>','noo-menu-select', $items );
				unset($items);
			}
			$output .= $this->toHtml($menu_object->term_id,$args);
				
			return $output;
		}
		
	}
	
}
new Noo_Menu();
