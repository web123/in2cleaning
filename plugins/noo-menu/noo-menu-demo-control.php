<?php

if(!function_exists('postbox_classes')){
	if(!is_admin()){
		function postbox_classes(){
			
		}
	}
}
global $noo_menu_style_id;
if (isset($_GET['style_id'])){
	$noo_menu_style_id = $_GET['style_id'];
	if(!defined('NOO_MENU_DEMO')){
		define('NOO_MENU_DEMO',$noo_menu_style_id);
	}
}

function noomenudemocontrolscripts(){
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-slider');
	wp_enqueue_script('jquery-ui-droppable');
	wp_enqueue_script('jquery-ui-sortable');
	
	wp_enqueue_script('noo-menu-bootstrap-tab',NOO_MENU_URL.'assets/js/bootstrap-tab.js',array('jquery'),'2.3.2');
	
	wp_enqueue_script('noo-menu-minicolors',NOO_MENU_URL.'assets/js/jquery.minicolors.custom.js',array(),NOO_MENU_VERSION);
	//wp_enqueue_script('noo-menu-updatemenu', NOO_MENU_URL.'assets/js/updatemenu.js', array(),NOO_MENU_VERSION);
	wp_enqueue_style('noo-menu-minicolors',NOO_MENU_URL.'assets/css/jquery.minicolors.css');
	wp_enqueue_style('noo-menu-demo',NOO_MENU_URL.'assets/css/demo.css');
}

require_once NOO_MENU_DIR.'includes/noo-menu-admin.php';

class NooMenuDemoControl {
	public function __construct(){
		add_shortcode('noomenudemocontrol', array($this,'demo_control'));
		if(!wp_is_mobile()){
			add_action("wp_print_scripts",'noomenudemocontrolscripts');
		}
	}
	
	public function demo_control($atts, $content = null){
		extract( shortcode_atts( array(
			'vertical'    => false,
		), $atts ) );
		$vertical = ($vertical === 'true') ? true : false;

?>
<div class="clearfix"></div>
<?php if(!$vertical) : ?>
<h2 class="control-title"><?php _e('Demo Control', NOO_MENU); ?></h2>
<?php endif; ?>
<div class="noo-demo-control noo-row">
	<div class="noo-span3">
		<?php if($vertical) : ?>
		<?php wp_nav_menu( array( 'theme_location' => 'vertical-menu', 'menu_class' => 'nav-menu' ) ); ?>
		<?php else : ?>
		<div id="poststuff">
			<div class="" id="preset">
				<h2><?php echo __('Preset Styles',NOO_MENU)?></h2>
				<form action="" id="persetForm" method="get">
					<div class="noo-row">
						<?php 
						$styles = get_option('noo_menu_style');
						foreach ($styles as $k=>$style){
						?>
						<div class="postbox" data-id="<?php echo $k ?>" style="cursor: pointer;"><h3><?php echo $style['name']?></h3></div>
						<?php
						}
						?>
					</div>
					<input name="style_id" type="hidden">
				</form>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="noo-span9 hidden-tablet">
		<?php if($vertical) : ?>
		<div id="poststuff">
			<div class="" id="preset">
				<h2 class="control-title"><?php echo __('Preset Styles',NOO_MENU)?></h2>
				<form action="" id="persetForm" method="get">
					<?php 
					$styles = get_option('noo_menu_style');
					$counter = 0;
					foreach ($styles as $k=>$style){
					?>
					<?php if(($counter % 3) == 0) : ?>
					<div class="noo-row">
					<?php endif; ?>
					<div class="postbox noo-span4" data-id="<?php echo $k ?>" style="cursor: pointer;"><h3><?php echo $style['name']?></h3></div>
					<?php if($counter % 3 == 2) : ?>
					</div>
					<?php endif; ?>
					<?php
						$counter++;
					}
					?>
					<input name="style_id" type="hidden">
				</form>
			</div>
		</div>
		<?php endif; ?>
		<?php if($vertical) : ?>
		<h2 class="control-title"><?php _e('Demo Control', NOO_MENU); ?></h2>
		<?php endif; ?>
		<table style="width: 100%">
			<tbody>
				<tr>
					<td scope="row" class="td-controls">
						<div id="poststuff">
							<div id="noo_menu_css_generator" class="metabox-holder clearfix">
								<ul class="tabs nav nav-tabs clearfix" id="noTabs">
									<li><a class="noo-custom-style-tab" data-toggle="tab" href="#general"><?php echo __('Animation',NOO_MENU) ?></a></li>
									<li class="active"><a class="noo-custom-style-tab noo-adv-ctr noo-menu-bar-style" data-toggle="tab" href="#tab1"><?php echo __('Menu Bar',NOO_MENU) ?></a></li>
									<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab2"> <?php echo __('Top Menu',NOO_MENU) ?></a></li>
									<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab3"> <?php echo __('Top Menu Hover',NOO_MENU) ?></a></li>
									<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab4"> <?php echo __('Sub Menu',NOO_MENU) ?></a></li>
									<li><a class="noo-custom-style-tab noo-adv-ctr" data-toggle="tab" href="#tab5"> <?php echo __('Sub Menu Hover',NOO_MENU) ?></a></li>
								</ul>
								<div id="tabsContent" class="tab-content">
									
									<div class="tab-pane fade" id="general">
									<br>
										<div class="noo-row">
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_animation_div" class="postbox <?php echo postbox_classes('noo_animation_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Animation',NOO_MENU)?></span></h3>
													<div class="inside">
														<p>
															<label for="animation"><?php echo __('Animation',NOO_MENU) ?></label>
															<select name="<?php echo noo_menu_get_option_name('animation')?>" id="animation" class="form-control">
																<option <?php if(noo_menu_get_option('animation') == ''): ?> selected="selected"<?php endif;?> value=""><?php echo __('None',NOO_MENU) ?></option>
																<option <?php if(noo_menu_get_option('animation') == 'fading'): ?> selected="selected"<?php endif;?>value="fading"><?php echo __('Fading',NOO_MENU) ?></option>
																<option <?php if(noo_menu_get_option('animation') == 'slide'): ?> selected="selected"<?php endif;?>value="slide"><?php echo __('Slide',NOO_MENU) ?></option>
																<option <?php if(noo_menu_get_option('animation') == 'zoom'): ?> selected="selected"<?php endif;?>value="zoom"><?php echo __('Zoom',NOO_MENU) ?></option>
																<option <?php if(noo_menu_get_option('animation') == 'elastic'): ?> selected="selected"<?php endif;?>value="elastic"><?php echo __('Elastic',NOO_MENU) ?></option>
															</select>
														</p>
														<?php noo_menu_element_slider('animation_duration','Duration','0','1000','400','ms') ?>
														<?php noo_menu_element_slider('animation_delay','Delay','0','1000','0','ms') ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade in active" id="tab1">
									<br>
										<div class="noo-row">
											<div class="noo-span4 meta-box-sortables">
												<?php if(noo_menu_get_option('orientation','horizontal')=='horizontal'):?>
												<div id="noo_menu_bar_height_div" class="postbox <?php echo postbox_classes('noo_menu_bar_height_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Menu Bar Height',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_slider('menu_bar_height','Height','','','40')?>
													</div>
												</div>
												<?php endif;?>
												<div id="noo_menu_bar_border_div" class="postbox <?php echo postbox_classes('noo_menu_bar_border_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Border',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_slider('menu_bar_border_width','Border Width',0,20)?>
														<p>
															<label for="border-style"><?php echo __('Border Style',NOO_MENU) ?> </label>
															<?php noo_menu_border_style('menu_bar')?>
														</p>
														
														<?php noo_menu_element_color('menu_bar_boder_color','Border Color')?>
														<?php noo_menu_border_position('menu_bar')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_menu_bar_background_div" class="postbox <?php echo postbox_classes('noo_menu_bar_background_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Background',NOO_MENU)?></span></h3>
													<div class="inside">
													<?php noo_menu_background('menu_bar',true,true,false)?>
													</div>
												</div>
												
												<div id="noo_menu_bar_border_radius_div" class="postbox <?php echo postbox_classes('noo_menu_bar_border_radius_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Radius Corners',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_meu_border_radius('menu_bar',false)?>
													</div>
												</div>
												<div id="noo_menu_bar_shadow_div" class="hide postbox <?php echo postbox_classes('noo_menu_bar_shadow_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Menu Bar Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('menu_bar')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_menu_bar_padding_div" class="postbox <?php echo postbox_classes('noo_menu_bar_padding_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
														<h3 class="hndle"><span><?php echo __('Padding',NOO_MENU)?></span></h3>
														<div class="inside">
														<?php noo_menu_paddings('menu_bar',false,true,false,true,0,0,0,0,'')?>
														</div>
												</div>
												<div id="noo_menu_bar_margin_div" class="postbox <?php echo postbox_classes('noo_menu_bar_margin_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Margin',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_margins('menu_bar',false,true,false,true,0,0,0,0,'')?>
													</div>
												</div>
											</div>
										</div>
									</div>
								
									<div class="tab-pane fade" id="tab2">
									<br>
										<div class="noo-row">
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_top_menu_height_div" class="postbox <?php echo postbox_classes('noo_top_menu_height_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Top Menu Height',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_slider('top_menu_height','Height','','','30')?>
														<?php if(noo_menu_get_option('orientation','horizontal')=='horizontal'):?>
														<?php noo_menu_element_slider('top_menu_vertical_position','Top Menu Vertical Position','','','0')?>
														<?php endif;?>
													</div>
												</div>
												<div id="noo_top_menu_border_div" class="postbox <?php echo postbox_classes('noo_top_menu_border_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Border',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_slider('top_menu_border_width','Border Width',0,20)?>
														<p>
															<label for="border-style"><?php echo __('Border Style',NOO_MENU) ?> </label>
															<?php noo_menu_border_style('top_menu')?>
														</p>
														<?php noo_menu_element_color('top_menu_boder_color','Border Color')?>
														<?php noo_menu_border_position('top_menu')?>
													</div>
												</div>
												
												
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_top_menu_font_div" class="postbox <?php echo postbox_classes('noo_top_menu_font_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Font',NOO_MENU)?></span></h3>
													<div class="inside">
														<p>
															<?php noo_menu_google_font('top_menu');?>
														</p>
														<p>
															<label for="top_menu_font_style"><?php echo __('Font Style',NOO_MENU) ?> </label>
																<select class="form-control" onchange="updatemenu()" id="top_menu_font_style" name="<?php echo noo_menu_get_option_name('top_menu_font_style')?>">
																  <option value="normal" <?php if(noo_menu_get_option('top_menu_font_style','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
																  <option value="italic" <?php if(noo_menu_get_option('top_menu_font_style','normal') == 'italic'):?>selected="selected"<?php endif;?>><?php echo __('Italic',NOO_MENU) ?></option>
																  <option value="oblique" <?php if(noo_menu_get_option('top_menu_font_style','normal') == 'oblique'):?>selected="selected"<?php endif;?>><?php echo __('Oblique',NOO_MENU) ?></option>
																</select>
														</p>
														<p>
															<label for="top_menu_font_weight"><?php echo __('Font Weight',NOO_MENU) ?> </label>
																<select class="form-control" onchange="updatemenu()" id="top_menu_font_weight" name="<?php echo noo_menu_get_option_name('top_menu_font_weight')?>">
																  <option value="normal" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
																  <option value="bold" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'bold'):?>selected="selected"<?php endif;?>><?php echo __('Bold',NOO_MENU) ?></option>
																  <option value="bolder" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'bolder'):?>selected="selected"<?php endif;?>><?php echo __('Bolder',NOO_MENU) ?></option>
																  <option value="lighter" <?php if(noo_menu_get_option('top_menu_font_weight','normal') == 'lighter'):?>selected="selected"<?php endif;?>><?php echo __('Lighter',NOO_MENU) ?></option>
																</select>
														</p>
														<?php noo_menu_element_slider('top_menu_font_size','Font Size','','','14')?>
														<?php noo_menu_element_color('top_menu_color','Font Color','#0088CC')?>
														<p>
															<input id="top_menu_font_uppercase" onchange="updatemenu()" type="checkbox" value="yes" <?php if(noo_menu_get_option('top_menu_font_uppercase') == 'yes'):?> checked="checked"<?php endif;?> name="<?php echo noo_menu_get_option_name('top_menu_font_uppercase')?>">
															<label for="top_menu_font_uppercase"><?php echo __('Use UPPERCASE',NOO_MENU)?></label>
														</p>
													</div>
												</div>
												<div id="noo_top_menu_font_shadow_div" class="postbox hide <?php echo postbox_classes('noo_top_menu_font_shadow_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Text Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('top_menu_font',false,false)?>
													</div>
												</div>
												
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_top_menu_padding_div" class="postbox <?php echo postbox_classes('noo_top_menu_padding_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Padding',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_paddings('top_menu',false,true,false,true,6,12,6,12,'')?>
													</div>
												</div>
												<div id="noo_top_menu_margin_div" class="postbox <?php echo postbox_classes('noo_top_menu_margin_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Margin',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_margins('top_menu',false,true,false,true,0,0,0,0,'')?>
													</div>
												</div>
												<div id="noo_top_menu_corner_radius_div" class="postbox <?php echo postbox_classes('noo_top_menu_corner_radius_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Corner Radius ',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_meu_border_radius('top_menu','')?>
													</div>
												</div>
												<div id="noo_top_menu_box_shadow_div" class="postbox hide <?php echo postbox_classes('noo_top_menu_box_shadow_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Box Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('top_menu')?>
													</div>
												</div>
											</div>
										</div>
									</div>
								
									<div class="tab-pane fade" id="tab3">
									<br>
										<div class="noo-row">
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_top_menu_hover_text_color_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_text_color_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Text Color',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_color('top_menu_hover_color','Color')?>
													</div>
												</div>
												<div id="noo_top_menu_hover_text_shadow_div" class="postbox hide <?php echo postbox_classes('noo_top_menu_hover_text_shadow_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Text Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('top_menu_hover_font',false,false)?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_top_menu_hover_background_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_background_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Background',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_background('top_menu_hover',true,true,'')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_top_menu_hover_border_color_div" class="postbox <?php echo postbox_classes('noo_top_menu_hover_border_color_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Border Color',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_color('top_menu_hover_boder_color','Color')?>
													</div>
												</div>
											</div>
										</div>
									</div>
								
									<div class="tab-pane fade" id="tab4">
									<br>
										<div class="noo-row">
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_sub_menu_box_border_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_border_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Box Border',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_slider('sub_menu_border_width','Border Width',0,20)?>
														<p>
															<label><?php echo __('Border Style',NOO_MENU) ?> </label>
															<?php noo_menu_border_style('sub_menu')?>
														</p>
														<?php noo_menu_element_color('sub_menu_border_color','Border Color')?>
														<?php noo_menu_border_position('sub_menu')?>
													</div>
												</div>
												
												<div id="noo_sub_menu_box_border_radius_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_border_radius_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Box Corner Radius',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_meu_border_radius('sub_menu','')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_sub_menu_font_div" class="postbox hide <?php echo postbox_classes('noo_sub_menu_font_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Font',NOO_MENU)?></span></h3>
													<div class="inside">
														<p>
															<?php noo_menu_google_font('sub_menu');?>
															
														</p>
														<p>
															<label for="sub_menu_font_style"><?php echo __('Font Style',NOO_MENU) ?> </label>
																<select class="form-control" onchange="updatemenu()" id="sub_menu_font_style" name="<?php echo noo_menu_get_option_name('sub_menu_font_style')?>">
																  <option value="normal" <?php if(noo_menu_get_option('sub_menu_font_style','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
																  <option value="italic" <?php if(noo_menu_get_option('sub_menu_font_style','normal') == 'italic'):?>selected="selected"<?php endif;?>><?php echo __('Italic',NOO_MENU) ?></option>
																  <option value="oblique" <?php if(noo_menu_get_option('sub_menu_font_style','normal') == 'oblique'):?>selected="selected"<?php endif;?>><?php echo __('Oblique',NOO_MENU) ?></option>
																</select>
														</p>
														<p>
															<label for="sub_menu_font_weight"><?php echo __('Font Weight',NOO_MENU) ?> </label>
																<select class="form-control" onchange="updatemenu()" id="sub_menu_font_weight" name="<?php echo noo_menu_get_option_name('sub_menu_font_weight')?>">
																  <option value="normal" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'normal'):?>selected="selected"<?php endif;?>><?php echo __('Normal',NOO_MENU) ?></option>
																  <option value="bold" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'bold'):?>selected="selected"<?php endif;?>><?php echo __('Bold',NOO_MENU) ?></option>
																  <option value="bolder" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'bolder'):?>selected="selected"<?php endif;?>><?php echo __('Bolder',NOO_MENU) ?></option>
																  <option value="lighter" <?php if(noo_menu_get_option('sub_menu_font_weight','normal') == 'lighter'):?>selected="selected"<?php endif;?>><?php echo __('Lighter',NOO_MENU) ?></option>
																</select>
														</p>
														<?php noo_menu_element_slider('sub_menu_font_size','Font Size','','','14')?>
														<?php noo_menu_element_color('sub_menu_color','Font Color','#333333')?>
														<p>
															<input id="sub_menu_font_uppercase" type="checkbox" onchange="updatemenu()" value="yes" <?php if(noo_menu_get_option('sub_menu_font_uppercase') == 'yes'):?> checked="checked"<?php endif;?> name="<?php echo noo_menu_get_option_name('sub_menu_font_uppercase')?>">
															<label for="sub_menu_font_uppercase"><?php echo __('Use UPPERCASE',NOO_MENU)?></label>
														</p>
													</div>
												</div>
												
												<div id="noo_sub_menu_text_shadown_div" class="postbox hide <?php echo postbox_classes('noo_sub_menu_text_shadown_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Text Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('sub_menu_font',false,false)?>
													</div>
												</div>
												
												<div id="noo_sub_menu_box_shadow_div" class="postbox hide <?php echo postbox_classes('noo_sub_menu_box_shadow_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Box Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('sub_menu_box')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												
												<div id="noo_sub_menu_box_background_div" class="postbox <?php echo postbox_classes('noo_sub_menu_box_background_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Box Background',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_background('sub_menu',true,true,'')?>
													</div>
												</div>
												<div id="noo_sub_menu_padding_div" class="postbox hide <?php echo postbox_classes('noo_sub_menu_padding_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Item Padding',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_paddings('sub_menu_item',true,true,true,true,5,20,5,20,'')?>
													</div>
												</div>
												<div id="noo_sub_menu_box_padding_div" class="postbox hide <?php echo postbox_classes('noo_sub_menu_box_padding_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Box Padding',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_paddings('sub_menu_box',true,true,true,true,5,5,5,5,'')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_sub_menu_border_div" class="postbox <?php echo postbox_classes('noo_sub_menu_border_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Sub Menu Item Border',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_slider('sub_menu_item_border_width','Border Width',0,20)?>
														<p>
															<label><?php echo __('Border Style',NOO_MENU) ?> </label>
															<?php noo_menu_border_style('sub_menu_item')?>
														</p>
														<?php noo_menu_element_color('sub_menu_item_border_color','Border Color')?>
														<?php noo_menu_border_position('sub_menu_item')?>
													</div>
												</div>
											</div>
										</div>
									</div>
								
									<div class="tab-pane fade" id="tab5">
									<br>
										<div class="noo-row">
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_sub_menu_hover_text_color_div" class="postbox <?php echo postbox_classes('noo_sub_menu_hover_text_color_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Text Color',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php 
														noo_menu_element_color('sub_menu_hover_color','Color')
														?>
													</div>
												</div>
												<div id="noo_sub_menu_hover_text_shadow_div" class="postbox hide <?php echo postbox_classes('noo_sub_menu_hover_text_shadow_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Text Shadow',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_shadow('sub_menu_hover_font',false,false)?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables">
												<div id="noo_sub_menu_hover_background_div" class="postbox <?php echo postbox_classes('noo_menu_bar_border_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Background',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_background('sub_menu_hover',true,true,'')?>
													</div>
												</div>
											</div>
											<div class="noo-span4 meta-box-sortables hide">
												<div id="noo_sub_menu_hover_boder_color_div" class="postbox <?php echo postbox_classes('noo_sub_menu_hover_boder_color_div', NOO_MENU) ?>">
													<div class="handlediv" title="<?php echo esc_attr__('Click to toggle') ?>"><br></div>
													<h3 class="hndle"><span><?php echo __('Hover Border Color',NOO_MENU)?></span></h3>
													<div class="inside">
														<?php noo_menu_element_color('sub_menu_item_hover_boder_color','Color')?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
function updatemenu(){
	var _ = jQuery,css='',container_id = '.noonav.' + _('.noonav').attr('id')+' ',container = '.noonav.' + _('.noonav').attr('id');

		css += container_id + '.noo-megamenu.animate .mega > .mega-dropdown-menu{';
		css += 'transition-duration:'+_('input#animation_duration').val()+'ms;';
		css += '-moz-transition-duration:'+_('input#animation_duration').val()+'ms;';
		css += '-webkit-transition-duration:'+_('input#animation_duration').val()+'ms;';
		
		css += 'transition-delay:'+_('input#animation_delay').val()+'ms;';
		css += '-moz-transition-delay:'+_('input#animation_delay').val()+'ms;';
		css += '-webkit-transition-delay:'+_('input#animation_delay').val()+'ms;';
		css +='}';
		
		//menu bar
		css += container_id + '.noo-megamenu {';
		css += 'border-radius:'+ _('input#menu_bar_radius').val() +'px;';
		css += '-moz-border-radius:'+ _('input#menu_bar_radius').val() +'px;';
		css += '-o-border-radius:'+ _('input#menu_bar_radius').val() + 'px;';
		
		if(_('input#menu_bar_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+'), to('+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#menu_bar_solid_background_color').data('rgbastring')+';'
		}
		
		_('#menu_bar_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+_this.val() +':'+_('input#menu_bar_border_width').val()+'px '+_('select#menu_bar_border_style').val()+' '+_('input#menu_bar_boder_color').val() +';';
			}
		});
	
		// css +='height:'+_('input#menu_bar_height').val()+'px;';
		css +='box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px '+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';\
				-webkit-box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px'+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';\
				-moz-box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px'+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';';
		
		css +='padding-right:'+_('input#menu_bar_padding_right').val()+'px;';
		css +='padding-left:'+_('input#menu_bar_padding_left').val()+'px;';
		css +='margin-right:'+_('input#menu_bar_margin_right').val()+'px;';
		css +='margin-left:'+_('input#menu_bar_margin_left').val()+'px;';
		
		css +='}';

		//menu bar
		css += container_id + '.noo-megamenu.horizontal {';
		css +='height:'+_('input#menu_bar_height').val()+'px;';
		css +='}';

		css += container+'.noo-nav-mb1{';
			css +='box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px '+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';\
			-webkit-box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px'+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';\
			-moz-box-shadow: '+_('input#menu_bar_h_shadow').val()+'px '+_('input#menu_bar_v_shadow').val()+'px '+_('input#menu_bar_b_shadow').val()+'px '+_('input#menu_bar_s_shadow').val()+'px'+_('input#menu_bar_shadow_color').data('rgbastring')+''+(_('input#menu_bar_shadow_inset').is(':checked') ? ' inset' : '')+';';
	
		css +='}';
		
		//container
		css += container_id +  '{';
		if(_('input#menu_bar_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+'), to('+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#menu_bar_gradient_background_start_color').data('rgbastring')+',  '+_('input#menu_bar_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#menu_bar_solid_background_color').data('rgbastring')+';'
		}
		css += 'border-radius:'+ _('input#menu_bar_radius').val() +'px;';
		css += '-moz-border-radius:'+ _('input#menu_bar_radius').val() +'px;';
		css += '-o-border-radius:'+ _('input#menu_bar_radius').val() + 'px;';
		css +='}';
		
		//Top menu
		css +=container_id + '.noo-nav > li > a{';
		css +='height:'+_('input#top_menu_height').val()+'px;';
		css +='line-height:'+_('input#top_menu_height').val()+'px;';
		
		css +='font-weight: '+_('select#top_menu_font_weight').val()+';';
		css +='font-family: '+_('select#top_menu_font').val()+';';
		css +='font-style: '+_('select#top_menu_font_style').val()+';';
		css +='font-size: '+_('input#top_menu_font_size').val()+'px;';
		css +='color: '+_('input#top_menu_color').val()+';';
		
		if(_('input#top_menu_font_uppercase').is(':checked')){
			css +='text-transform: uppercase;';
		}
		_('#top_menu_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +':'+_('input#top_menu_border_width').val()+'px '+_('select#top_menu_border_style').val()+' '+_('input#top_menu_boder_color').val() +';';
			}
		});
	
		css +='text-shadow: '+_('input#top_menu_font_h_shadow').val()+'px '+_('input#top_menu_font_v_shadow').val()+'px '+_('input#top_menu_font_b_shadow').val()+'px '+_('input#top_menu_font_shadow_color').data('rgbastring')+';';
		
		css +='box-shadow: '+_('input#top_menu_h_shadow').val()+'px '+_('input#top_menu_v_shadow').val()+'px '+_('input#top_menu_b_shadow').val()+'px '+_('input#top_menu_s_shadow').val()+'px '+_('input#top_menu_shadow_color').data('rgbastring')+''+(_('input#top_menu_shadow_inset').is(':checked') ? ' inset' : '')+';\
				-webkit-box-shadow: '+_('input#top_menu_h_shadow').val()+'px '+_('input#top_menu_v_shadow').val()+'px '+_('input#top_menu_b_shadow').val()+'px '+_('input#top_menu_s_shadow').val()+'px '+_('input#top_menu_shadow_color').data('rgbastring')+''+(_('input#top_menu_shadow_inset').is(':checked') ? ' inset' : '')+';\
				-moz-box-shadow: '+_('input#top_menu_h_shadow').val()+'px '+_('input#top_menu_v_shadow').val()+'px '+_('input#top_menu_b_shadow').val()+'px '+_('input#top_menu_s_shadow').val()+'px '+_('input#top_menu_shadow_color').data('rgbastring')+''+(_('input#top_menu_shadow_inset').is(':checked') ? ' inset' : '')+';';

		
		//css +='padding-top:'+_('input#top_menu_padding_top').val()+'px;';
		css +='padding-right:'+_('input#top_menu_padding_right').val()+'px;';
		//css +='padding-bottom:'+_('input#top_menu_padding_bottom').val()+'px;';
		css +='padding-left:'+_('input#top_menu_padding_left').val()+'px;';
		
		css +='margin-top:'+_('input#top_menu_vertical_position').val()+'px;';
		
		css +='margin-right:'+_('input#top_menu_margin_right').val()+'px;';
		css +='margin-left:'+_('input#top_menu_margin_left').val()+'px;';
		css +='border-radius:'+ _('input#top_menu_radius').val() +'px;\
			-moz-border-radius:'+ _('input#top_menu_radius').val() +'px;\
			-o-border-radius:'+ _('input#top_menu_radius').val() + 'px';
		css +='}';
		
		css += container_id +' .navbar-brand{';
		css +='color: '+_('input#top_menu_color').val()+';';
		css +='}';
		//caret
		css += container_id + '.noo-megamenu .caret{';
	//	css +='border-bottom-color: '+_('input#top_menu_color').val()+';';
		css +='border-top-color: '+_('input#top_menu_color').val()+';';
		css +='}';
		
		//top menu hover
		css +=container_id + '.noo-nav > li:hover > a,'+container_id  + '.noo-nav > li.open > a,'+container_id + '.noo-nav > .current-menu-item > a,'+container_id + ' .noo-nav > .current-menu-ancestor > a,'+container_id + ' .noo-nav > .current_page_item > a,'+container_id + ' .noo-nav > .current_page_ancestor > a{';
		
		_('#top_menu_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +'-color:' +_('input#top_menu_hover_boder_color').val() +';';
			}
		});
		
		css +='color: '+_('input#top_menu_hover_color').val()+';';
		if(_('input#top_menu_hover_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+'), to('+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#top_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#top_menu_hover_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#top_menu_hover_solid_background_color').data('rgbastring')+';'
		}
		
		css +='text-shadow: '+_('input#top_menu_hover_font_h_shadow').val()+'px '+_('input#top_menu_hover_font_v_shadow').val()+'px '+_('input#top_menu_hover_font_b_shadow').val()+'px '+_('input#top_menu_hover_font_shadow_color').data('rgbastring')+';';
		css +='}';
		
		css +=container_id +'.noo-nav li.dropdown.open .caret,'+container_id +' .noo-nav li.dropdown.open.active .caret,'+container_id +' .noo-nav li.dropdown.open a:hover .caret,'+container_id +'.noo-nav .dropdown-toggle:hover .caret,'+container_id +'.noo-nav > li:hover > a > .caret,'+container_id +'.noo-nav > .current-menu-item > a > .caret,'+container_id +' .noo-nav > .current-menu-ancestor > a > .caret,'+container_id +' .noo-nav > .current_page_item > a > .caret,'+container_id +' .noo-nav > .current_page_ancestor > a > .caret{';
		css +='border-top-color: '+_('input#top_menu_hover_color').val()+';';
		css +='border-bottom-color: '+_('input#top_menu_hover_color').val()+';';
		css +='}';
		
		css += '@media (max-width: 767px) {'+container_id +' .noo-nav > li.mega > a:after{border-color:'+_('input#top_menu_color').val()+' rgba(0, 0, 0, 0)} '+container_id +' .noo-nav > li.mega:hover > a:after,'+container_id +' .noo-nav > li.mega.open > a:after,'+container_id +' .noo-nav > li.mega.current-menu-item > a:after,'+container_id +' .noo-nav > li.mega.current_page_item > a:after,'+container_id +' .noo-nav > li.mega.current_page_ancestor > a:after{border-color:'+_('input#top_menu_hover_color').val()+' rgba(0, 0, 0, 0)}}';

		//sub menu
		css +=container_id +'.noo-megamenu .dropdown-menu,'+container_id +'.noo-megamenu .dropdown-submenu > .dropdown-menu{';
		css +='border-radius:'+ _('input#sub_menu_radius').val() +'px;\
			-moz-border-radius:'+ _('input#sub_menu_radius').val() +'px;\
			 -o-border-radius:'+ _('input#sub_menu_radius').val() + 'px;';
		if(_('input#sub_menu_solid_background').is(':checked')){
			css += 'background-color:'+ _('input#sub_menu_solid_background_color').data('rgbastring')+';'
		}else{
			css +=' background: linear-gradient(top,  '+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+'), to('+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#sub_menu_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_gradient_background_end_color').data('rgbastring')+');';
		}
		
		_('#sub_menu_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +':'+_('input#sub_menu_border_width').val()+'px '+_('select#sub_menu_border_style').val()+' '+_('input#sub_menu_border_color').val() +';';
			}
		});
		
		css +='box-shadow: '+_('input#sub_menu_box_h_shadow').val()+'px '+_('input#sub_menu_box_v_shadow').val()+'px '+_('input#sub_menu_box_b_shadow').val()+'px '+_('input#sub_menu_box_s_shadow').val()+'px '+_('input#sub_menu_box_shadow_color').data('rgbastring')+''+(_('input#sub_menu_box_shadow_inset').is(':checked') ? ' inset' : '')+';\
			-webkit-box-shadow: '+_('input#sub_menu_box_h_shadow').val()+'px '+_('input#sub_menu_box_v_shadow').val()+'px '+_('input#sub_menu_box_b_shadow').val()+'px '+_('input#sub_menu_box_s_shadow').val()+'px '+_('input#sub_menu_box_shadow_color').data('rgbastring')+''+(_('input#sub_menu_box_shadow_inset').is(':checked') ? ' inset' : '')+';\
			-moz-box-shadow: '+_('input#sub_menu_box_h_shadow').val()+'px '+_('input#sub_menu_box_v_shadow').val()+'px '+_('input#sub_menu_box_b_shadow').val()+'px '+_('input#sub_menu_box_s_shadow').val()+'px '+_('input#sub_menu_box_shadow_color').data('rgbastring')+''+(_('input#sub_menu_box_shadow_inset').is(':checked') ? ' inset' : '')+';';

		css +='padding: '+_('input#sub_menu_box_padding_top').val()+'px '+_('input#sub_menu_box_padding_right').val()+'px '+_('input#sub_menu_box_padding_bottom').val()+'px '+_('input#sub_menu_box_padding_left').val()+'px;';
	
		css +='}'; 
		
		css += container_id + '.noo-megamenu .dropdown-menu .mega-nav > li > a{';
		css +='font-weight: '+_('select#sub_menu_font_weight').val()+'px;';
		css +='font-family: '+_('select#sub_menu_font').val()+';';
		css +='font-style: '+_('select#sub_menu_font_style').val()+';';
		css +='font-size: '+_('input#sub_menu_font_size').val()+'px;';
		css +='color: '+_('input#sub_menu_color').val()+';';
		if(_('input#sub_menu_font_uppercase').is(':checked')){
			css +='text-transform: uppercase;';
		}
		css +='text-shadow: '+_('input#sub_menu_font_h_shadow').val()+'px '+_('input#sub_menu_font_v_shadow').val()+'px '+_('input#sub_menu_font_b_shadow').val()+'px '+_('input#sub_menu_font_shadow_color').data('rgbastring')+';';
		css +='padding: '+_('input#sub_menu_item_padding_top').val()+'px '+_('input#sub_menu_item_padding_right').val()+'px '+_('input#sub_menu_item_padding_bottom').val()+'px '+_('input#sub_menu_item_padding_left').val()+'px;';
		_('#sub_menu_item_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +':'+_('input#sub_menu_item_border_width').val()+'px '+_('select#sub_menu_item_border_style').val()+' '+_('input#sub_menu_item_border_color').val() +';';
			}
		});
		css += '}';
		
		//sub menu hover
		css += container_id +'.noo-megamenu .dropdown-menu .mega-nav > li:hover > a:not(.mega-group-title){';
		
		_('#sub_menu_item_border_position').find('input').each(function(){
			var _this = _(this);
			if(_this.is(':checked')){
				css +='border-'+ _this.val() +'-color:' +_('input#sub_menu_item_hover_boder_color').val() +';';
			}
		});
		
		css +='color: '+_('input#sub_menu_hover_color').val()+';';
		
		if(_('input#sub_menu_hover_gradient_background').is(':checked')){
			css +=' background: linear-gradient(top,  '+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -ms-linear-gradient(top,  '+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+');\
					background: -webkit-gradient(linear, left top, left bottom, from('+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+'), to('+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+'));\
					background: -moz-linear-gradient(top,  '+_('input#sub_menu_hover_gradient_background_start_color').data('rgbastring')+',  '+_('input#sub_menu_hover_gradient_background_end_color').data('rgbastring')+');';
		}else{
			css += 'background-color:'+ _('input#sub_menu_hover_solid_background_color').data('rgbastring')+';'
		}
		css +='text-shadow: '+_('input#sub_menu_hover_font_h_shadow').val()+'px '+_('input#sub_menu_hover_font_v_shadow').val()+'px '+_('input#sub_menu_hover_font_b_shadow').val()+'px '+_('input#sub_menu_hover_font_shadow_color').data('rgbastring')+';';
		
		css += '}';
//	}	
css += '@media (max-width: 767px) {'+container_id +'.noo-megamenu .dropdown-menu .mega-nav > li > a:after{border-color:'+_('input#sub_menu_color').val()+' rgba(0, 0, 0, 0)} '+container_id +' .noo-megamenu .dropdown-submenu > a:after,'+container_id +' .noo-megamenu .mega-group > a:after{border-color:'+_('input#top_menu_hover_color').val()+' rgba(0, 0, 0, 0)}}';

	_("#noo-menu-custom-css").remove();
	_('#noo-menu-default-css').next('style').after(_('<style id="noo-menu-custom-css" type="text/css">'+css+'</style>'));
	return css;
}
jQuery(document).ready(function($){

	<?php if(!wp_is_mobile()):?>
	
	$('select#animation').on('change',function(){
		$('.noo-megamenu').removeClass("animate fading slide zoom elastic").addClass('animate').addClass($(this).val());
	});

	$('select.google-font').on('change',function(){
		var id = $(this).attr('id');
		var url = $(this).find('option:selected').data('url');
		$(this).next('input').val(url).attr('value',url);
		var rel = '<link id="noo-menu-google-font'+id+'" media="all" type="text/css" href="//fonts.googleapis.com/css?'+url+'" rel="stylesheet">';
		$("#noo-menu-google-font"+id).remove();
		$(rel).appendTo('head');
	});
	

	$('select.google-font').each(function(){
		var id = $(this).attr('id');
		var url = $(this).find('option:selected').data('url');
		$(this).next('input').val(url).attr('value',url);
		var rel = '<link id="noo-menu-google-font'+id+'" media="all" type="text/css" href="//fonts.googleapis.com/css?'+url+'" rel="stylesheet">';
		$("#noo-menu-google-font"+id).remove();
		$(rel).appendTo('head');
	});
	
	$(".slider").each(function(){
		var $this = $(this),
			run = $this.data('run') || 0 ;
		if($this.attr('id') == 'top_menu_height'){
			$this.data('max',$('input#menu_bar_height').val());
		}
		$this.slider({ 
			range:  "min",
			value:  $this.next('input').val() || 0,
			min: $this.data('min') || 0,
			max: $this.data('max') || 100,
			slide: function(event, ui) {
				$this.parent().find('span#'+$this.attr('id')).text(ui.value);
				$this.next('input').val(ui.value).attr('value',ui.value);
				if(run >= 0){
					updatemenu();
				}
			},
			stop: function(event,ui){
				
				if($this.attr('id') == 'menu_bar_height'){
					if(parseInt($('input#menu_bar_height').val()) < parseInt($('input#top_menu_height').val()) ){
						$('div#top_menu_height').slider('value',$('input#menu_bar_height').val());
						$('div#top_menu_height').parent().find('span#'+ $('div#top_menu_height').attr('id')).text($('input#menu_bar_height').val());
						$('div#top_menu_height').next('input').val($('input#menu_bar_height').val()).attr('value',$('input#menu_bar_height').val());
					}

					
					
					$('div#top_menu_height').slider('option','max',$('input#menu_bar_height').val());
					
				}
				
				
				if($('input#menu_bar_height').val() <= $('input#top_menu_height').val()){
					$('div#top_menu_vertical_position').parent().find('span#'+ $('div#top_menu_vertical_position').attr('id')).text(0);
					$('div#top_menu_vertical_position').next('input').val(0).attr('value',0);
					$('div#top_menu_vertical_position').slider('value',0);
					$('div#top_menu_vertical_position').slider('disable');
				}else{
					$('div#top_menu_vertical_position').slider('enable');
					var max_ver_pos = parseInt($('input#menu_bar_height').val()) - parseInt($('input#top_menu_height').val());
					if($('div#top_menu_vertical_position').next('input').val() > max_ver_pos){
						$('div#top_menu_vertical_position').parent().find('span#'+ $('div#top_menu_vertical_position').attr('id')).text(max_ver_pos);
						$('div#top_menu_vertical_position').next('input').val(max_ver_pos).attr('value',max_ver_pos);
						$('div#top_menu_vertical_position').slider('value',max_ver_pos);
					}
					$('div#top_menu_vertical_position').slider('option','max', max_ver_pos);
				}
				if(run >= 0){
					updatemenu();
				}
			}
		});
	});	
	$(".color").each(function(){
		var $this = $(this);
		$this.minicolors({
			control: $this.attr('data-control') || '',
			defaultValue: $this.attr('data-defaultValue') || '',
			inline: $this.attr('data-inline') === 'true',
			letterCase: $this.attr('data-letterCase') || 'lowercase',
			opacity: $this.attr('data-opacity'),
			position: $this.attr('data-position') || 'bottom left',
			change: function(hex, opacity) {
				if( !hex ) return;
				if( opacity ) {
					var id = $this.attr('id');
					$('#'+id+'_opacity').val(opacity).attr('value',opacity);
				}
			},
			theme: 'bootstrap'
		});
	});
	<?php endif;?>
	$('#preset').find('.postbox').each(function(){
		$(this).click(function(){
			$('#persetForm').find('input[name=style_id]').prop('value',$(this).data('id'));
			$('#persetForm').submit();
		});
	});
	
	updatemenu();
});
</script>
<?php
	}
}
new NooMenuDemoControl();